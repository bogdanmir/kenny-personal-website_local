---
title: Now
description: Here’s an overview of what I’m doing now
og_title: About Kenny Grant
og_description: Here’s an overview of what I’m doing now
tw_title: About Kenny Grant
tw_description: Here’s an overview of what I’m doing now:
image: images/preview.png
layout: now
---
