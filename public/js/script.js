(function($){
  if ($(".navbar-burger").is(':visible')) {
    $(".navbar-burger").click(function() {
      $(".navbar-menu").toggleClass("is-active");
      $(".navbar-brand").toggleClass("is-active");
      // toggle content visibility when navbar open
      $("#content-container").toggleClass("hide-content");
    })
  }

  $(window).resize(function(e) {
    if (!$(".navbar-burger").is(':visible') && $(".navbar-item.has-dropdown.is-active").length) {
      $(".navbar-item.has-dropdown.is-active").removeClass('is-active');
    }
  });

  function resizeSwiper() {
    $('.swiper-container').height($('.swiper-slide img').height())
  }

  var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      breakpointsInverse: true,
    },
    breakpoints: {
      769: {
        centeredSlides: true,
        autoHeight: true,
        calculateHeight:true,
      }
    },
    effect: 'fade',
    autoplay: {
      delay: 5000,
      disableOnInteraction: true,
    },
    onImagesReady: function (swiper) {
     resizeSwiper();
     swiper.onResize();
   }
  });

  $(window).resize(function(){
    resizeSwiper()
  });
})(jQuery);

document.addEventListener("DOMContentLoaded", function() {
  var lazyloadImages;    

  if ("IntersectionObserver" in window) {
    lazyloadImages = document.querySelectorAll(".lazy");
    var imageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        setTimeout(function() {
          if (entry.isIntersecting) {
            var image = entry.target;
            image.src = image.dataset.src;
            image.classList.remove("lazy");
            imageObserver.unobserve(image);
          }
        }, 1500)
      });
    });

    lazyloadImages.forEach(function(image) {
      imageObserver.observe(image);
    });
  } else {  
    var lazyloadThrottleTimeout;
    lazyloadImages = document.querySelectorAll(".lazy");
    
    function lazyload () {
      if(lazyloadThrottleTimeout) {
        clearTimeout(lazyloadThrottleTimeout);
      }    

      lazyloadThrottleTimeout = setTimeout(function() {
        var scrollTop = window.pageYOffset;
        lazyloadImages.forEach(function(img) {
            if(img.offsetTop < (window.innerHeight + scrollTop)) {
              img.src = img.dataset.src;
              img.classList.remove('lazy');
            }
        });
        if(lazyloadImages.length == 0) { 
          document.removeEventListener("scroll", lazyload);
          window.removeEventListener("resize", lazyload);
          window.removeEventListener("orientationChange", lazyload);
        }
      }, 20);
    }

    document.addEventListener("scroll", lazyload);
    window.addEventListener("resize", lazyload);
    window.addEventListener("orientationChange", lazyload);
  }
})