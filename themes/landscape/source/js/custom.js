function showAllPosts() {
    document.getElementById('all_posts').style.display = 'block';
    document.getElementById('limited_posts').style.display = 'none';
}

function onLoadNewsLetter(event) {
  var emailField = document.getElementById("newsLetterEmail");
  var form = document.getElementById("mailChimpForm");
  if(emailField.value && emailField.checkValidity()) {
    form.submit()
  };
}

function maichimpSubmission(event) {
  $.ajax({
    type: 'GET',
    url: 'https://kenny.us6.list-manage.com/subscribe/post?u=6b98493cc2e9ddaae1c981401&amp;id=d6c184c4d4',
    data: $('#mailChimpForm').serialize(),
    dataType: 'jsonp',
    success: function(response) {
      console.log(response)
    },
  })
}

$('#mailchimpForm').on('submit', function(e) {
  e.preventDefault()
  console.log(e)
  $('.response-error').hide()
  $.ajax({
    type: 'GET',
    url: 'https://kenny.us6.list-manage.com/subscribe/post-json?u=6b98493cc2e9ddaae1c981401&amp;id=d6c184c4d4&c=?',
    data: $('#mailchimpForm').serialize(),
    dataType: 'jsonp',
    success: function(response) {
      console.log(response)
      if (response.result == 'success') {
        $('.m-form').hide()
        $('.response').show()
      } else {
        var msg = response.msg
        $('.response-error').show()
        if (response.msg.includes('The domain portion of the email address is invalid')) {
          msg = 'Invalid email address. Please try again'
        }

        if (response.msg.includes('is already subscribed')) {
          msg = 'This email is already subscribed'
        }

        if (response.msg.includes('Please enter a value')) {
          msg = 'Please fill out this field'
        }

        if (response.msg.includes('An email address must contain')) {
          msg = 'Invalid email address. Please try again'
        }

        if (response.msg.includes('The username portion of the email address is empty')) {
          msg = 'Invalid email address. Please try again'
        }
        $('.response-error-text').text(msg)
      }
    },
  })
})