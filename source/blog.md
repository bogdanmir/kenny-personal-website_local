---
title: My Blog - Kenny Grant
description: Read my latest thoughts and findings.
og_title: My Blog - Kenny Grant
og_description: Read my latest thoughts and findings.
tw_title: My Blog - Kenny Grant
tw_description: Read my latest thoughts and findings.
image: images/blog-kenny-social-preview2.jpg
layout: blog
---
