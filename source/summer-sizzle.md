---
title: Summer Sizzle mix from Kenny Grant
description: Listen to my dj mix titled Summer Sizzle, a collection of my favorite tracks from this summer. Best served poolside.
og_title: Summer Summer Sizzle mix from Kenny Grant
og_description: Collection of my favorite tracks from this summer. Best served poolside.
tw_title: Summer Summer Sizzle mix from Kenny Grant
tw_description: Collection of my favorite tracks from this summer. Best served poolside.
image: images/summer-sizzle-fb-social-preview.jpg
layout: summer-sizzle

---