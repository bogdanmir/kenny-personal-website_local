---
title: Kenny Grant, Entrepreneur and Business Builder living in Vancouver and Singapore
description: I love bringing ideas to life and building ways to turn those ideas into products. I split my time between Vancouver and Singapore. Learn more about what makes me tick and what businesses i'm building. 
og_title: About Kenny Grant
og_description: I love bringing ideas to life and building ways to turn those ideas into products. I split my time between Vancouver and Singapore. Learn more about what makes me tick and what businesses i'm building.
tw_title: About Kenny Grant
tw_description: I love bringing ideas to life and building ways to turn those ideas into products. I split my time between Vancouver and Singapore. Learn more about what makes me tick and what businesses i'm building.
image: images/about-kenny-grant-facebook-preview-image.jpg
layout: about
swiperImages: 
- [images/slide/kenny-grant-jakarta-conference.jpg, kenny grant jakarta conference]
- [images/slide/kenny-grant-bangkok-golden-arches.jpg, Paddy fields]
- [images/slide/kenny-grant-bali-grass-padddy-fields.jpg, kenny grant bali grass padddy fields]
- [images/slide/kenny-grant-light-shine-down.jpg, kenny  grant light shine down]
- [images/slide/kenny-grant-eyes-wide.jpg, kenny grant eyes wide]
- [images/slide/team-photo-staged-by-pool.jpg, team photo staged by pool]
galleryImage1: images/gallery-1.png
galleryImage2: images/gallery-1.png
galleryImage3: images/gallery-1.png
galleryImage4: images/gallery-1.png
galleryImage5: images/gallery-1.png
galleryImage6: images/about-gallery/kenny-half-marathon.jpg
galleryImage7: images/about-gallery/scooter-greece.jpg
galleryImage8: images/about-gallery/soccer-champions-urban-rec.jpg
galleryImage9: images/gallery-1.png
galleryImage10: images/gallery-1.png
galleryImage11: images/gallery-1.png
galleryImage12: images/gallery-1.png
galleryImage13: images/gallery-1.png
galleryImage14: images/gallery-1.png
galleryImage15: images/gallery-1.png
galleryImage16: images/gallery-1.png
galleryImage17: images/gallery-1.png
---
