---
title: Queen of the North mix from Kenny Grant
description: A 65 minute mix of deep, soulful, groovin, and jacking house music mixed by your truly.
og_title: Queen of the North DJ mix
og_description: A 65 minute mix of deep, soulful, groovin, and jacking house music mixed by Kenny Grant. 
tw_title: Queen of the North DJ mix
tw_description: A 65 minute mix of deep, soulful, groovin, and jacking house music mixed by Kenny Grant. 
image: images/queen-of-the-north-social-preview.jpg
layout: queen-of-the-north

---