---
title: My Reading List - Kenny Grant
description: Check out my book list.
og_title: My Reading List - Kenny Grant
og_description: Check out my book list.
tw_title: My Reading List - Kenny Grant
tw_description: Check out my book list.
image: images/blog-kenny-social-preview2.jpg
layout: reading
---
