---
title: Builder | Kenny Grant
description: About Kenny Grant
og_title: About Kenny Grant
og_description: About Kenny Grant
tw_title: About Kenny Grant
tw_description: About Kenny Grant
image: images/preview.png
layout: builder
banner_desktop: images/builder-desktop.png
banner_mobile: images/builder-mobile.png
---