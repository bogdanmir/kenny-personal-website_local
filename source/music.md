---
title: House Music from Kenny Grant
description: Here are some of my house music dj mixes
og_title: House Music from Kenny Grant
og_description: Here are some of my house music dj mixes
tw_title: House Music from Kenny Grant
tw_description: Here are some of my house music dj mixes
image: images/music-homepage-social-preview.jpg
layout: music
swiperImages: 
- [images/banner.png, Paddy fields]
- [images/banner.png, Paddy fields]
- [images/banner.png, Paddy fields]
- [images/banner.png, Paddy fields]
- [images/banner.png, Paddy fields]
- [images/banner.png, Paddy fields]
- [images/banner.png, Paddy fields]
galleryImage1: images/gallery-1.png
galleryImage2: images/gallery-1.png
galleryImage3: images/gallery-1.png
galleryImage4: images/gallery-1.png
galleryImage5: images/gallery-1.png
galleryImage6: images/gallery-1.png
galleryImage7: images/gallery-1.png
galleryImage8: images/gallery-1.png
galleryImage9: images/gallery-1.png
galleryImage10: images/gallery-1.png
galleryImage11: images/gallery-1.png
galleryImage12: images/gallery-1.png
galleryImage13: images/gallery-1.png
galleryImage14: images/gallery-1.png
galleryImage15: images/gallery-1.png
galleryImage16: images/gallery-1.png
galleryImage17: images/gallery-1.png
---