---
title: Kenny Grant, social impact entrepreneur and product leader living in Vancouver and Singapore
description: I love bringing digital products to life. Learn more about what makes me tick.
og_title: About Kenny Grant
og_description: I love bringing digital products to life. Learn more about my mission, what makes me tick, and what businesses i'm building next. 
tw_title: About Kenny Grant
tw_description: I love bringing digital products to life. Learn more about my mission, what makes me tick, and what businesses i'm building next.

image: images/about-nice-to-meet-you-social-preview2.jpg

layout: about
swiperImages:
- [images/kenny-grant-coffee-smile.jpg, kenny grant working in cafe]
- [images/slide/opt/kenny-grant-bangkok-golden-arches.jpg, Paddy fields]
- [images/slide/opt/kenny-grant-bali-grass-padddy-fields.jpg, kenny grant bali grass padddy fields]
- [images/slide/opt/team-photo-staged-by-pool.jpg, team photo staged by pool]
- [images/kenny-rollerblading.jpg, kenny grant rollerblading]



galleryImages:
- [images/placeholders/1130x800.jpeg, 'alt txt']
- [images/placeholders/800x800.jpeg, 'alt txt']
- [images/placeholders/800x800.jpeg, 'alt txt']
- [images/placeholders/1130x800.jpeg, 'alt txt']
- [images/placeholders/1940x800.jpeg, 'alt txt']
- [images/placeholders/800x800.jpeg, 'alt txt']
- [images/placeholders/800x800.jpeg, 'alt txt']
- [images/placeholders/800x800.jpeg, 'alt txt']
- [images/placeholders/1610x700.jpeg, 'alt txt']
- [images/placeholders/1610x700.jpeg, 'alt txt']
- [images/placeholders/800x1410.jpeg, 'alt txt']

galleryImage12: images/bottom-about/bali-will-smith-kanban-board-opt.jpg
galleryImage16: images/bottom-about/beautiful-british-columbia-opt.jpg
galleryImage17: images/bottom-about/la-beach-view-opt.jpg
galleryImage18: images/bottom-about/beach-tree-boat.jpg
galleryImage19: images/bottom-about/mini-malibu-surfboard-opt.jpg

---
