---
title: May Newsletter - Issue 7
post_title: "May Newsletter - Issue 7"
post_description: "Welcome to my personal newsletter. Welcome to Issue #07"
og_title: "May Newsletter - Issue 07"
og_description: "Welcome to my personal newsletter. Welcome to Issue #07"
tw_title: "May Newsletter - Issue 07"
tw_description: "Welcome to my personal newsletter. Welcome to Issue #07"
image: /May-Newsletter-Issue-07/covid-wall-art-vancouver.jpg
date: 2020-05-02 07:00:09


---



<div class="container">
<img alt="covid wall mural art vancouver" src="/May-Newsletter-Issue-07/covid-wall-art-vancouver.jpg" class="full-width">


<p>April was crazy. I kicked off 2 new products, killed Unfurls, and paused work with a Vancouver based marketplace. I registered a new BC Corp and started working with several new contractors from around the world.</p>

<p>How are you holding up? I'm in a good flow right now. It's been heartwarming working with these new teammates, hearing Covid stories from Toronto, Calgary, New York, SF, London, Jakarta, Singapore, and Paris. Regardless of location, everyone is rallying. Here in Vancouver, local artists have transformed shuttered business storefronts into beautiful murals. I've captured some of my favourites at the end of this post, but they all share the same message of thanks and hope.</p>

<p>Overall, I'm feeling pretty energized about the coming months. I hope you are keeping sane and productive.</p>

<p>Here's my update:</p>


</div>
<hr>

<br>
</div>

<div class="container">
<h2>Gratitude Survey Results</h2>
<p>Ran a quick survey on Twitter a few weeks back. I've since transitioned surveys to Google Forms, but check out the results below. I might move everything to airtable or surveymonkey. Results below:
</p>

<img alt="do you practice gratitude survey" src="/May-Newsletter-Issue-07/twitter-gratitude-survey.png" class="full-width">



<p>Surprised? What frequency and techniques do you practice?</p>

<p>If expressing gratitude <a href="https://www.health.harvard.edu/healthbeat/giving-thanks-can-make-you-happier">makes you happier</a>, how can we encourage others to practice more of it?  </p>

</div>
<hr>

<br>
</div>

<div class="container">
<h2>New Mobile App = PUTJ:</h2>

<p>Codename PUTJ. Kicked off a new mobile application to encourage gratitude. We completed a 2-week design sprint and started development last week. This is my first React Native application and eager to see how this pans out. Android and iOS drop June. I'm <a href="https://airtable.com/shrIvyo5PmfEjkQgx">looking for early adopters</a>. Trying to make this 110% fun, and a bit over the top. Big shouts to Simon for making this possible. </p>

<p>Product slogan: <i>"Share gratitude with the world, live happier"</i></p>
 
<p>🚢🚢🚢🚢</p>

</div>
<hr>

<br>
</div>

<div class="container">
<h2>Shutting down Unfurls.co</h2>
<p>Last weekend I refunded all active subscribers, logged into Heroku and deleted everything. It was bittersweet.</p>
<p><a href="/shutting-down-unfurls">Read the full post</a></p>


</div>
<hr>

<br>
</div>

<div class="container">
<h2>UK Mood Tracker</h2>
<img alt="uk mood tracker during covid" src="/May-Newsletter-Issue-07/uk-mood-tracker-during-covid.png" class="full-width">
<p>I absolutely love this initiative. Happiness levels across the UK are <a href="https://yougov.co.uk/topics/science/trackers/britains-mood-measured-weekly">slowly returning</a> to normal?</p>

</div>
<hr>

<br>
</div>

<div class="container">
<h2>Feelings Wheel</h2>
<img alt="Feelings Wheel" src="/May-Newsletter-Issue-07/feelings-wheel.jpg" class="full-width">
<p>see full size here - > <a href="http://feelingswheel.com/">http://feelingswheel.com</a> - shoutout to my friend Max for sharing this link!</p>
</div>
<hr>

<div class="container">


<h2>April Updates:</h2>

* Covid-19.
* Streaks: 106 days without coffee. Eating an insane amount of chocolate and carbs these days. 
* I just started a 60-minute daily meditation challenge, anyone want to join?
* <a href="https://www.myyogateacher.com/">My Yoga Teacher</a> - 1 on 1 yoga lessons over live video. Pick time, teacher, share your goals, and they will cater a class to your specific needs. I thought this would be weird, but it was radical. Free trial, check it out!
* MSCHF is at it again. Think Banksy, but of internet digital properties. <a href="https://severedspots.com/">Painted dots</a> for thousands, sold out in minutes. This latest drop is spotless.
* Learning to cook or want some kitchen inspiration? <a href="https://www.facebook.com/NuNuRestaurant/posts/2428694370565037">Nick from Nunu</a> posts new videos every few days. I I worked with Nick as a kitchenhand while backpacking Australia in my early 20's. Phenomenal chef. Thoughtful and interesting videos.
* <a href="https://www.nytimes.com/2018/11/05/style/self-care/the-calm-place-on-the-internet.html">A calm place</a> by NYT. You're welcome. 
* <a href="https://www.youtube.com/watch?v=ctuByt_4eRc&feature=youtu.be">Mission Human Flight - China Tianmen Mountain | 4K</a> - okay, sorry fam, but this is on my bucket list
* Top <a href="https://www.sachinrekhi.com/top-resources-for-product-managers">90 resources</a> for Product Managers
* <a href="https://a16z.com/2020/04/18/its-time-to-build/">It's time to build</a> - Marc Andreessen
* How, What, and Where to Build. Inspired by "it's time to build", here's a framework called <a href="https://breakingsmart.substack.com/p/how-what-and-where-to-build">the builder’s cone</a> to think about it in a useful historical context.
* Enjoyed this episode of <a href="https://www.youtube.com/watch?v=DBrPItK3Hco">The Random Show - Kevin Rose + Tim Tim</a>
* <a href="https://twitter.com/herdyshepherd1/status/1255825917026471936">Spent the last hour judging the online sheep show</a>
* <a href="https://twitter.com/BrianRoemmele/status/1254059433098797059">Lynx cats have serious hops</a>
* <a href="https://techcrunch.com/2020/04/18/clubhouse-app-chat-rooms/">Clubhouse voice chat</a> is gaining popularity
* <a href="https://medium.com/@m2jr/how-to-build-a-breakthrough-3071b6415b06">How to build a breakthrough</a>
* Stanford researchers devise a new form of <a href="http://med.stanford.edu/news/all-news/2020/04/stanford-researchers-devise-treatment-that-relieved-depression-i.html">magnetic brain stimulation treatment</a> that relieved depression in 90% of participants. 
* Tweet: <a href="https://twitter.com/monsterreese/status/1253487074759581697">My wife “took” me on a date to the movies. It was stupid cute and VERY expensive</a>. This is super cute, and your partner would love it
* <a href="https://mcusercontent.com/6b98493cc2e9ddaae1c981401/images/de8ed9fa-8bda-4821-84ce-9156aa409f37.jpeg">Tokyo Metro stations offer 15-minute work stations</a>. $2.50 for 15 minutes. We can expect to see these everywhere in the coming years. Thx Nate. 
* Hold my beer <a href="https://twitter.com/holdmyale/status/1247763609163202561">while I cross this water</a>.
* Steve Jobs' brain at work. <a href="https://twitter.com/wisdom_theory/status/1250722471579398144">15 seconds of thinking before speaking</a>.
* City Building. What can we learn from Walt Disney's grand vision for EPCOT and the steps he took? How might those lessons help those looking to build the next city of tomorrow? <a href="https://www.perell.com/fellowship/walt-disney-city-architect">A rather epic take</a>.
* <a href="https://www.technologyreview.com/2017/07/21/242297/the-emerging-science-of-computational-psychiatry/">The Emerging Science of Computational Psychiatry</a> - VR experiment to measure social rejection.

</div>
<hr>

<div class="container">


<h2>Personal:</h2>

<strong>Books:</strong> Reading<br>

* <a href="https://amzn.to/2yoJFQp">Principles</a>
* <a href="https://amzn.to/3ddtFQt">How to Be Like Walt</a>
* <a href="https://amzn.to/2vsMVsn">The Great CEO Within</a>

<strong>Books:</strong> Just Purchased<br>
* <a href="https://amzn.to/3d86SFE">Indistractable; How to Control Your Attention and Choose Your Life</a>
* <a href="https://amzn.to/2SsOrU1">Creativity, Inc: Overcoming the Unseen Forces That Stand in the Way of True Inspiration</a>
* <a href="https://amzn.to/2SsOrU1">Pour Your Heart Into It: How Starbucks Built a Company One Cup at a Time</a>
* <a href="https://amzn.to/3ddtFQt">How to Be Like Walt</a>
* <a href="https://amzn.to/3fb9X9B">Let My People Go Surfing: The Education of a Reluctant Businessman</a>
* <a href="https://amzn.to/3dbISS1">Walt Disney: The Triumph of the American Imagination</a>
* <a href="https://amzn.to/2xv612C">Wooden: A Lifetime of Observations and Reflections On and Off the Court</a>
<br>
<p>^^ Not allowed to order any more books</p>
<br> 

<strong>Film/TV</strong><br>

* Masterclass:<a href="https://www.masterclass.com/classes/bob-iger-teaches-business-strategy-and-leadership">Business Strategy and Leadership with Bob Iger</a>, <a href="https://www.masterclass.com/classes/chris-voss-teaches-the-art-of-negotiation">Art of Negotiation with Chris Voss</a>
* <a href="https://www.imdb.com/title/tt11823076/">Tiger King - 😮
* <a href="https://www.imdb.com/title/tt8420184/">The Last Dance</a> - I don't watch basketball, but this Michael Jordan documentary is fascinating and rocket fuel inspiration for peak performers.


<strong>Music</strong><br>

<p>Djing on twitch. Funk and disco from the 70s and 80s fill my ears.</p><br>

* <a href="https://www.mixcloud.com/SoulCoolRecords/kenny-dope-gonzalez-roller-boogie-80s/">Kenny Dope - Gonzalez roller boogie 80s</a> - 114 minutes of baby-making music
* <a href="https://www.youtube.com/watch?v=x7mbtEoTFV8">Pleasure - let me be the one</a>
* <a href="https://www.youtube.com/watch?v=PD3u5P08UUE">Disclosure - Tondo</a>
* <a href="https://www.youtube.com/watch?v=VpC637CBmBM">Alicia Myers - I Want to thank you</a> - fire
* <a href="https://www.youtube.com/watch?v=dICJbNRtW_g">Mariah Carey - Touch My Body (Cyril Hahn Remix)</a>

</div>
<hr>

<br>
</div>

<div class="container">

<p>Let me know you made it this far. Reply in the comments... What's your fav Covid activity? What's good in your world?</p>

Thanks for reading,<br>
Kenny<br>
<a href="http://twitter.com/allbombs">twitter.com/allbombs</a>
<br>

</div>
<hr>

<br>
</div>


<div class="container">

<h2>A few photos from April</h2> 




<img alt="thankyoumural" src="/May-Newsletter-Issue-07/thankyoumural.jpg" class="full-width">
<img alt="thank you who would you like to thank" src="/May-Newsletter-Issue-07/thank-you-who-would-you-like-to-thank.jpg" class="full-width">
<img alt="empathy mural" src="/May-Newsletter-Issue-07/empathy-mural.jpg
" class="full-width">
<img alt="thank you doctors nurses" src="/May-Newsletter-Issue-07/thank-you-doctors-nurses.jpg" class="full-width">
<img alt="do the best you can" src="/May-Newsletter-Issue-07/do-the-best-you-can.jpg" class="full-width">
<img alt="be kind" src="/May-Newsletter-Issue-07/be-kind.jpg" class="full-width">
<img alt="we are one thank you" src="/May-Newsletter-Issue-07/we-are-one-thank-you.jpg" class="full-width">
<img alt="cartoon covid face mask" src="/May-Newsletter-Issue-07/cartoon-covid-face-mask.jpg" class="full-width">
<img alt="thank you rainbow" src="/May-Newsletter-Issue-07/thank-you-rainbow.jpg
" class="full-width">
<img alt="we are in this together mural" src="/May-Newsletter-Issue-07/we-are-in-this-together-mural.jpg" class="full-width">
<img alt="we love you vancouver" src="/May-Newsletter-Issue-07/we-love-you-vancouver.jpg" class="full-width">
<img alt="stand united 2m apart" src="/May-Newsletter-Issue-07/stand-united-2m-apart.jpg" class="full-width">
<img alt="granville street doctor mural" src="/May-Newsletter-Issue-07/granville-street-doctor-mural.jpg" class="full-width">
<img alt="life is short art is long mural" src="/May-Newsletter-Issue-07/life-is-short-art-is-long-mural.jpg" class="full-width">
<img alt="always essential mural" src="/May-Newsletter-Issue-07/always-essential-mural.jpg" class="full-width">
<img alt="dr bonnie bc vancouver mural" src="/May-Newsletter-Issue-07/dr-bonnie-bc-vancouver-mural.jpg" class="full-width">
<img alt="soon we will get to hold each other" src="/May-Newsletter-Issue-07/soon-we-will-get-to-hold-each-other.jpg" class="full-width">
<img alt="we are all in this together art mural" src="/May-Newsletter-Issue-07/we-are-all-in-this-together-art-mural.jpg" class="full-width">
<img alt="room in order mural" src="/May-Newsletter-Issue-07/room-in-order-mural.jpg" class="full-width">
<img alt="kenny covid work out room living room" src="/May-Newsletter-Issue-07/kenny-covid-work-out-room-living-room.jpg" class="full-width">
<img alt="kenny dj pioneer nexus" src="/May-Newsletter-Issue-07/kenny-dj-pioneer-nexus.jpg" class="full-width">
<img alt="dj house music live on twitch" src="/May-Newsletter-Issue-07/dj-house-music-live-on-twitch.jpg" class="full-width">


</div>
