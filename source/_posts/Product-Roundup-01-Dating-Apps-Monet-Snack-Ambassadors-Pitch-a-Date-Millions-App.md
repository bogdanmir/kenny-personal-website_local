---
title: Product Roundup 01 Dating Apps Monet Snack Ambassadors Pitch a Date Millions App
post_title: "Product Roundup 01 Dating Apps Monet Snack Ambassadors Pitch a Date Millions "
post_description: "Welcome to my first product roundup"
og_title: "Product Roundup 01 Dating Apps, Monet, Snack, Pitch A Date, Ambassadors, Millions App"
og_description: "Welcome to my first product roundup"
tw_title: "Product Roundup 01 Dating Apps, Monet, Snack, Pitch A Date, Ambassadors, Millions App"
tw_description: "Welcome to my first product roundup"
image: /Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/roundup-01-twittercard.jpg
date: 2021-02-14 01:00:09


---



<div class="container">



<p>Happy Valentine's Day, everyone! Welcome to my Product Roundup. It's the first one, and I’m trying to figure out the right format. I dogfood tons of new products, so why not document them? In this roundup, we have three dating apps and two mini-games. A special shoutout to three of the five companies that are built locally in the Pacific Northwest!</p>


<p>Going back to Valentine's Day and the dating app scene, Bumble just went public. Whitney Wolfe Herd made history this past Thursday as the youngest female CEO to take a US company public. Bumble raised $2.2 billion in an initial public offering that exceeded expectations and valued the firm over $7 billion. In 2020, consumers spent over $3 billion on dating apps. That's up 15% YoY globally, seeing a total of 560 million dating app downloads. Check out the mobile dating stats from App Annie:</p>

<img alt="top dating apps by consumer spend" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/top-dating-apps-by-consumer-spend.png" class="full-width">

<img alt="top dating apps by avg mau 2020 " src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/top-dating-apps-by-avg-mau-2020.png" class="full-width">

<img alt="average weekly hours spent in dating apps usa" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/average-weekly-hours-spent-in-dating-apps-usa.png" class="full-width">


<p>Let's jump into some of the new dating applications trying to capture that growth. </p>

</div>


<hr>



<div class="container">


<h2>Monet</h2>
<h3>Draw Your Way to Love, or New Friends</h3>

<p>Monet went viral in December from <a href="https://www.tiktok.com/@joanna.shan/video/6883220655790181638?_d">this TikTok reel</a>. From there, Monet quickly captured 10k test flight users, secured a round of funding, launched on the App store, and started a thriving discord community of artists.</p>

<p>How does it work? Scroll through profiles, draw potential match something beautiful or horrendous. Match, chat, date, and save unique art to your public profile page. All I can think of is “Drawsomething meets Tinder”... or maybe https://skribbl.io/ vibes.  I'm not exactly sure if this is a dating app or a way to meet new friends, but I’m pretty confident I was the oldest person on the app. I met some new friends from around the world and found tech and creative focused.</p>



<p>I asked potential matches to draw a "cheeseburger singing" or "whatever makes their heart sing."</p>

<img alt="Monet World" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/monet-datin-swipe.jpg" class="full-width">
<img alt="Monet World" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/heart-sings-monet-1.jpg" class="full-width">
<img alt="Monet World" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/cheeseburger-art.jpg" class="full-width">


<p>There are oodles of small details in the product that brought a smile to my face. One example.. Monet created a viral growth hack of inviting friends to unlock new paint colours. Unsure if it’s working or not, but I thought it was creative. I thoroughly enjoyed testing Monet and give it two thumbs up. Check out some of my lolart</p>

<p></p>

<img alt="Monet World" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/monet-world3.jpg" class="full-width">


<p>Website: <a href="https://monet.world/">monet.world</a>
<br>App Store: <a href="https://apps.apple.com/us/app/monet-draw-for-dates-friends/id1535020150">iPhone</a>
</p>


</div>
<hr>

<div class="container">


<h2>Snack App</h2>

<img alt="Snack App Homepage" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/get-snack-app.png" class="full-width">


<p>I found <a href="https://www.thesnackapp.com/">Snack</a>, a local video-driven dating app that describes itself as "Tinder meets TikTok.” Founded by Kim Kaplan, an early team member at Plenty Of Fish that match.com acquired in 2015 for 575 million. They’ve already rebranded from Muse to Snack, raised a round of financing, and currently crowdsourcing a new logo. Do you have design chops? <a href="https://www.tiktok.com/@getsnackapp/video/6922964588002675974">Submit your logo</a> to win 10k. </p>

<p>Linkedin reads, "Snack as a well-funded company creating a new app for Gen Z.” Here's a screenshot from the homepage:</p>

<img alt="Snack App Homepage" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/snack-app-screenshot.png" class="full-width">


<p>After creating my account, uploading profile videos, I was taken to a video reel closely resembling TikTok. For the first version, you can swipe or like. It’s prelaunch, and I continue to see the same 10-15 profiles in my reel. Within the profile screen, it shows I’m one of the first 250 people on the app. It would be unfair for me to comment further, but I’m curious where they’ll go next. Snack reminds me of Duet, but focused on dating versus monetization.</p>

<img alt="Snack App screenshot reel" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/snack.jpg" class="full-width">
<img alt="Snack App screenshot profile" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/snack2.jpg" class="full-width">



<p>Video-based prompts could be fascinating for finding a match or just breaking the ice? Let’s see what Kim and the team serves up next.  </p>

<p>Website: <a href="https://www.thesnackapp.com/">Snack App</a>
<br>App Store: <a href="https://apps.apple.com/us/app/snack-video-dating/id1545836962">iPhone</a>
</p>

</div>
<hr>



<div class="container">


<h2>Pitch A Date</h2>

<p></p>

<p>Crowdsource your dating life. Attach bounty rewards for anyone that referrers to a hot date. Founded by Lucy Guo and Rose Xi, I’m calling this more of a marketing drop versus an actual dating service? Lucy posted her profile page with a whopping 1 million dollar reward for anyone that helps her find the one. Linkbait? Perhaps. Either way, here I am typing keystrokes writing about it, so it worked. How much $ would you spend on finding the one? Is there much difference between a professional matchmaking service? What about your privacy? Find out more, or pitch a date <a href="https://www.pitchadate.com/">here</a>. </p>


<p>Edit. Found this blurb from the co-founders:</p>
<p><i>Your own version of The Bachelor(ette). Every week, we'll feature bachelor(ette)s you can pitch a date to. We'll live stream some of the finalists. Even if you're not interested in dating one of our bachelor(ette)s, you can matchmake them for prizes.</i>


<img alt="pitch-a-date-home-page.png" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/pitch-a-date-home-page.png" class="full-width">
<img alt="bachelors-of-the-week.png" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/bachelors-of-the-week.png" class="full-width">


<img alt="pitch-a-date-win-1-million" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/pitch-a-date-win-1-million.png" class="full-width">

<img alt="pitch-a-date-testimonials" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/pitch-a-date-testimonials.png
" class="full-width">
<img alt="pitch-a-date-profile-view1" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/pitch-a-date-profile-view1.png" class="full-width">
<img alt="pitch-a-date-profile-view-2" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/pitch-a-date-profile-view-2.png
" class="full-width">


<p>Website: <a href="https://www.pitchadate.com/">pitchadate.com</a>
<br>App Store: n/a
</p>



</div>
<hr>






<div class="container">


<h2>Ambassadors</h2>

<p>A friend (name retracted) based out of British Columbia invited me to his new pet project. There's no website, just a testlight build for friends and fam. I'm unsure if Ambassadors is the real name, but the last round was called cashew? Whatever the case, it's what I’ve been playing for the previous few weeks. What's the game? Think Risk meets <a href="https://fingeronthe.app/">finger on the app</a>.

<img alt="Risk finger on the app" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/risk-finger-on-the-app-roundup1.jpg" class="full-width">



<p>At first glance, the game appears almost too simplistic; a single board with just 12 tappable images. I'm not much of a gamer, but I’ll attempt to summarize the concept.</p>

<p>Teams spawn with 450 points. Every four hours, you receive 45 taps to defend your position or attack an opposing team. You earn diamonds for slaying another group below each 100 point threshold and even more diamonds for knocking another team out of the game. When teams reach 99 points or less, they become "supercharged,” meaning every tap is 5x more powerful. <a href="https://youtu.be/0KjFN3-OuV0">Watch Campbell fight</a> team Ray.</p>

<img alt="Ambassadors screenshot" src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/Ambassadors-screenshot.jpg" class="full-width">


<p>You can invite additional teammates in exchange for diamonds! Strategies shift as teams add new teammates. You'll want to fire up iMessage or Signal to coordinate planned strikes. Campbell and I got a bit carried away. Go Team Axe! </p>

<p>Version 1 is a blast, and I send my congratulations to (name retracted). It kept me engaged for days. Do you want to play in the next cohort? email me</p>



<p>Website: n/a
<br>App Store: send me DM via twitter
</p>


</div>
<hr>


<div class="container">




<h2>Millions App</h2>

<img alt="Millions App " src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/million-dollar-app-screenshot-homepage.png" class="full-width">

<p>MyCard Inc, a fintech startup that recently raised 3m, launched an online sweepstakes game as a means to acquire new users. Here’s a blurb from Techcunrch:</p>

<p><i>Instead of weekly live games, users follow the Twitter account @millions, which either does a drop of some sort or gives away money to its followers every month. This month, for example, the account is launching its “million dollar sweepstakes.” Users follow @millions on Twitter, visit Millions.app, then enter six numbers. If all six match, they win $1 million</i></p>

<p>I wouldn’t call this an app, but more of a marketing drop. The number picker and user experience are pretty fluid, but imho they could go wayy bigger with surprise and delight. I’m unsure of the target audience, but why not encourage creativity with each submission? Why Twitter? The creator space is exploding right now.</p>

<p>The odds of matching all 6 numbers in 1 drawing are 1 in 1,120,529,256.</p>

<img alt="Millions App " src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/million-dollar-app-screenshot-pick6-numbers-blank.png" class="full-width">
<img alt="Millions App " src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/million-dollar-app-screenshot-pick6-numbers.png" class="full-width">
<img alt="Millions App " src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/million-dollar-app-screenshot-almost-done.png" class="full-width">
<img alt="Millions App " src="/Product-Roundup-01-Dating-Apps-Monet-Snack-Ambassadors-Pitch-a-Date-Millions-App/million-dollar-app-screenshot-in-it-to-win.png" class="full-width">


<p>Website: <a href="http://millions.app">millions.app</a> /
<br>App Store: n/a
</p>


<p>That's the end of today’s round up. Have any ideas or suggestions for the next one? Should I move this to a short 2-minute video? </p> 







</div>
<hr>

<br>
</div>



