---
title: "What are the best ways to launch a brand in Asia or Southeast Asia?"
post_title: "What are the best ways to launch a brand in Asia or Southeast Asia?"
post_description: "learn more about e-commerce marketplaces in Asia and how to launch a brand in the region."
og_title: "What are the best ways to launch a brand in Asia or Southeast Asia?"
og_description: "learn more about e-commerce marketplaces in Asia and how to launch a brand in the region."
tw_title: "What are the best ways to launch a brand in Asia or Southeast Asia?"
tw_description: "learn more about e-commerce (marketplaces) in Asia, and how to launch a brand in the region."
image: /social-preview.png
date: 2022-03-01 12:17:02
---


<div class="container">



<p>Are you looking to learn more about marketplaces and e-commerce in Asia? Expanding or launching an international brand into the region? This blog post is for you. The experience of exporting goods and entering a new market is challenging, in fact, more daunting. Especially in a region with different cultures, buying habits, native language, marketing channels, logistics, and time zones. Let's dive in!</p>


<p>What are the best ways to launch a brand in Asia or Southeast Asia?</p>
<p>Ditch plans for building a website, it's all about marketplaces and social commerce. </p>


<p><strong>In this blog post:</strong></p>

1. Why Asia?
1. Asia vs North America Ecommerce Buying Behaviours
1. How to launch or export into the region
1. Marketplaces & Airtable Resource
1. Call to action
1. Need help?

<br>
</div>

<hr>
<br>
<br>

<div class="container">
<h2>Why Asia?</h2>


<p>It's booming. E-commerce in Asia is incredibly large and growing rapidly, which makes it stand out from other parts of the world. Here are a few stats that might catch your attention:</p>


<p><br>

<blockquote> China has the largest digital buyer population in the world, amounting to more than 780 million people. It is the largest e-commerce market globally, generating almost 50 percent of the world’s transactions. According to eMarketer, China’s online retail transactions reached more than 710 million digital buyers, and transactions reached $2.29 trillion in 2020, with forecasts to reach $3.56 trillion by 2024.</blockquote>
<br>

<blockquote> In 2022, ecommerce sales in Southeast Asia will reach nearly $90 billion, with almost 65% of the region’s sales coming from Indonesia. The Philippines will lead the region in growth for the next several years.</blockquote>
<br>
<blockquote>The number of online shoppers in Southeast Asia will reach 380 million by 2026. The region’s e-commerce sector saw massive growth of almost 600 percent – climbing from US$5.5 billion to US$38 billion between 2015 and 2019.</blockquote>
<br>
<blockquote>Almost two-thirds of the Indonesian population is 40 years old and under, with a median age of 29 years. In addition, 87% of youth aged 6 to 11 have a smartphone – significantly above the global average of 69%. </blockquote>
<br>
<i> 
*Sources <a href="https://www.emarketer.com/content/southeast-asia-ecommerce-forecast-2022#page-report">1</a>,<a href="https://www.statista.com/forecasts/1283912/global-revenue-of-the-e-commerce-market-country">2</a>, <a href="">3</a></i>


 </p>
<br>


<strong>Total Population:</strong>
<p>Combined (Asia + SE Asia) = 2,28b 🤯🤯</p>


<img alt="Asia & SE Asia Population by country" src="/What-are-the-best-ways-to-launch-a-brand-in-Asia-or-Southeast-Asia/Asia_SE_Asia_Population-2022.png" class="full-width">
<figcaption>Asia & SE Asia Population by Country. Updated March, 2022 - <a href="https://docs.google.com/spreadsheets/d/1kjC3FLK6s2K3LLpdDXB8mfa3UyEKrC0_Q1FIC1MIGqk/edit?usp=sharing">Google Spreadsheet</a>.
</figcaption>
<br>

</div>

<br>
<hr>
<br>






<div class="container">


<h2>Asia vs North America Ecommerce Buying Behaviours</h2>


<p>While many western users have progressed from desktop computers to laptops to smartphones, many Asian users have never owned a PC. A mobile device is not just a user's first computer but also their only computer. Device affordability and bandwidth availability (LTE and 5g internet) usher in millions of new internet users peach month. Asia has a mobile-only approach and treats desktops as an afterthought. With that in mind, it's easy to understand how both business owners and consumers approach online e-commerce. Consumers predominately use social networks and online marketplaces to source and purchase products. They rarely visit a brand's website to make purchases because they simply don't exist.</p>

<p>Businesses start building an online presence via social networks and marketplaces. It's 100% free. Once they reach critical mass on several marketplaces, they may develop a dedicated mobile application, but it’s quite rare to develop a standalone dedicated website. </p>

<p>Why are both consumers and business owners predominantly using marketplaces? </p>
<p>Discoverability, distribution, customer support, managing payments, and the cost of launching an online business with a marketplace are non existent. Businesses don't know any better. Why would they create a website if it's out of the norm? Locals use social media and marketplaces to source products. Follow your customer.</p>

<br>

<p>How did local marketplaces seize the landrush opportunity? </p>

<p>Marketplaces flooded the market with a winner takes all mentality. They raised 100's millions of dollars from venture capitalists, family offices, and the local government. Those funds were aggressively allocated to marketing, ecommerce education (helping customers make their very first online purchase), and incentivizing business owners and consumers with steep discounts. Several marketplaces offered online business coaching, online workshops, and business tools such as inventory management + mini websites derived from your marketplace listing.</p>

<p>Another major factor international website builders and marketplaces were unable to activate the SE Asia revolves around accepting payments, credit cards, and language localization. </p>

<br>
<p><strong>Payments</p></strong>
<p>In SE Asia, the concept of credit cards is foreign. Adoption is <i>slow</i> with just a small percentage of the general population owning a credit card. Instead, they "top up" digital wallets from telecommunication companies and Super Apps like Go-Jek that are widely regarded as a digital bank. Go-jek started as a scooter rideshare marketplace (think uber) and quickly morphed into a digital wallet offering transport, food, cleaning, and banking. </p>

<p>When booking a flight or hotel reservation with Tokopedia (expedia equivalent), the confirmation screen includes a reservation ID with a countdown timer to complete payment off-site. From there, the potential buyer transfers funds from a mobile banking app, or visits a local convenience store to deposit funds. After X hours, those orders without a cleared payment are cancelled. </p>

<p>This checkout flow applies to e-commerce. When ordering from an Indonesian marketplace, a Go-jek driver picks up the product, delivers, and collects payment on arrival. Everything is fully integrated, with the buyer paying COD, or transferring funds using a digital wallet. Credit cards are rarely used. </p>

<p>It's a few extra clicks, but credit card payments and logistics are huge hurdles for existing website builders and international marketplaces. Can you imagine buying a website domain without a credit card? It's too complicated. Marketplaces won.</p>



<p><strong>Email marketing or email campaigns are non-existent.</p></strong>
<p>Asking customers for an email address return blank stares. Where did you regsiter your first email address? I'm guessing your cable tv provider transitioned to an ISP that offered 5-10 complementary emails as part of a monthly package. A strategic business iniative that provided some value, but was initiated to lock you in for the long haul. "If I switch to AT&T, I'll lose my email address"</p>

<p>Coming online for the first time? Why would you need an email address in an emerging market? What's a desktop? Everything is mobile + local SIM card.  Your phone number and social media account is your defacto standard communication channel and your personal identity. It's everything. There's no segregation, it's used for both pleasure and work. Email doesn't really exist.</p>

<p>Brands interact with customers via social networks, messaging platforms, and sms marketing. WhatsApp for business and Tiktik commerce are incredibly popular.</p>



<p>In China, Wechat Pay is integrated everywhere, including the on and offline world. Mini apps and marketplaces integrate with WePay. It’s the standard defacto. </p>

<p>Overall, the market in China is more mature, but marketplaces, social commerce, group buying, and live commerce dominate the scene. IMHO, the world’s best social commerce products stem from this region of the world. </p>


<p>Social media is a crucial component of a good marketing strategy in Asia, particularly important for maximizing brand awareness and attracting consumers. Social proof holds higher proof than that of the North American market.</p>
<br>


<p>If you want to go deeper, check the following <a href="/What-are-the-best-ways-to-launch-a-brand-in-Asia-or-Southeast-Asia/SE-Asia-digital-transformation.pdf">report from FB, Bain & Company</a>. At the very least, it's interesting if only to appreciate its sheer size and pace.</p>



 </p>

</div>

<hr>
<br>

<div class="container">


<h2>How to launch or export into the region</h2>

<p>
Expanding to Asia, or running an expansion test isn't for the faint of heart. It requires patience and a proper budget to garner success. And you might want to unlearn your western ecommerce playbook and mindset?

General tips:
- Research. Allocate ample time to build an expansion plan and budget. 
- Ditch any plans for a localized website. 
- Find a competitor on a local marketplace and social network that closely resembles your product and target - audience. How do they market and position themselves? What platforms, social networks, and channels do they leverage? What's working, what's not? Could this work for your brand? Do you have the resources and local expertise?
- Use the airtable <a href="https://airtable.com/shr107yJHUKNY1Sb7">marketplace resource provided</a> within this blog post to identity a marketplace that matches your demo, vertical, and region. (scroll below)
- Start with one or two target countries. Use this <a href="https://docs.google.com/spreadsheets/d/1kjC3FLK6s2K3LLpdDXB8mfa3UyEKrC0_Q1FIC1MIGqk/edit?usp=sharing">spreadsheet to sort by age + population</a>. 
- Conduct your own independent research and homework!
- Capture UGC and lifestyle photographs and video specific to the region. Assets perform differently for each marketplace and country. Your North American photoshoots or UGC generally don't perform well in Asia. 
- Hire marketplace experts or a small agency that specializes in international expansion. Consider rev share agreements based on performance.


<br>
<p><strong>TLDR</strong> - Forget building a localized website and become a local marketplace expert. Roll up your sleeves, or hire a consultant or specialized agency to save you $ and time.</p>

<br>
</div>
<hr>
<br>
<br>
<div class="container">


<h2>Marketplaces & Airtable Resource</h2>

<br>

</div>

<iframe class="airtable-embed" src="https://airtable.com/embed/shr107yJHUKNY1Sb7?backgroundColor=cyan&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>


<br>
<div class="container">

<p> View Airtable in full screen here - <a href="https://airtable.com/shr107yJHUKNY1Sb7">https://airtable.com/shr107yJHUKNY1Sb7</a>
</p>


<br>
<hr>
<br>


<div class="container">


<h2>Stuck? Need Help? Feedback?</h2>

<p>If launching a brand in the region, building a marketplace for the region, or if I missed an important Airtable entry, I’d love to chat. As always, my <a href="https://twitter.com/allbombs">DMs are open</a> or <a href="https://calendly.com/allbombs/ecom-building-intro-call">book a quick call</a>.</p>


<p>I hope this kickstarts your journey into the Asian market. It's a young, vibrant, and your customers are incredibly digital-savvy. Have fun =)

<p>If you found value, please share it with a colleague or on social media! And if you end up launching in the region, I'd love to hear your story and view your marketplace listing.</p>


 </p>


<div class="container">


</div>
<hr>
<br>



</div>
