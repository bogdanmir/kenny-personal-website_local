---
title: March Newsletter - Issue 5
post_title: "March Newsletter - Issue 5"
post_description: "Welcome to my personal newsletter. Welcome to Issue #05"
og_title: "March Newsletter - Issue 5"
og_description: "Welcome to my personal newsletter. Welcome to Issue #05"
tw_title: "March Newsletter - Issue 5"
tw_description: "Welcome to my personal newsletter. Welcome to Issue #05"
image: /March-Newsletter-Issue-05/sunshine-false-creek.jpg
date: 2020-02-29 07:00:09


---



<div class="container">
<img alt="sunshine false creek vancouver" src="/March-Newsletter-Issue-05/sunshine-false-creek.jpg" class="full-width">


<p>Happy leap year. 
Due to Covid-19, I canceled my plans to travel to Southeast Asia. Meaning, i'm stay put in Vancouver for now. The majority of February was spent working on a local marketplace startup. Still can’t share much, but launching this summer in the Greater Vancouver area before expanding to the rest of Canada. I'm looking for a second consulting and advisory role. Do you know anyone that needs help? I'm looking for a UX Designer and Business Analyst (Junior Product Manager). Remote, pt or ft. Anyone come to mind? Lmk.</p>


</div>


<hr>

<div class="container">


<h2>February Updates:</h2>

* My sister had a baby! Check out Bennett in the photo stream below.
* Streaks: 46 days without coffee, completed 30 day workout challenge.
* Started running last week. Thinking about <a href="https://bmovanmarathon.ca/halfmarathon">BMO half marathon</a> in May. 
* What makes people happier than money? Yale and Oxford <a href="https://thehill.com/changing-america/well-being/mental-health/482958-what-makes-people-happier-than-money-according-to">scientists have the answer</a>. 
* Airpods owner? Did you notice the recent update where <a href="https://support.apple.com/en-ca/HT210406">Siri automatically reads incoming text messages</a>? Audio feeds are going to be a new thing. Audio based summaries from 3rd party apps or services. 
* How many of <a href="https://storage.googleapis.com/zebraiq/Zebra%20IQ%202019%20The%20State%20of%20Gen%20Z.pdf">these Gen Z terms</a> do you know? Scroll to page 22 of the PDF. I scored 3. 
* <a href="https://twitter.com/engineeringvids/status/1233599389844869120">Video from 100 years ago</a> shows a car with “parking assist” technology 
* <a href="https://www.sachinrekhi.com/3-types-of-product-managers-builders-tuners-innovators">3 Types of Product Managers: Builders, Tuners, Innovators</a>
* <a href="https://www.sachinrekhi.com/finding-product-culture-fit">Finding Product Culture Fit</a> - advice for product managers in the midst of contemplating their next role
* <a href="https://firstround.com/review/after-15-years-as-a-product-leader-ceo-and-now-vc-heres-the-advice-i-always-share-with-future-founders/">After 15 Years as a Product Leader, CEO and Now VC, Here’s the Advice I Always Share with Future Founders<a/>
* <a href="https://startbotnet.com/">Botnet</a> is a social network simulator where you're the only human along with a million bots who are obsessed with you. 
* If you had to pick one <a href="https://www.scottadamssays.com/2012/02/01/the-right-priority/">priority in your life</a>, could you do it?
* Face-recognition respirator masks. "Our masks are custom printed with your face making phone access easy during viral epidemics" - <a href="https://faceidmasks.com/">https://faceidmasks.com/<a/>
* <a href="https://www.youtube.com/watch?v=vpcUVOjUrKk">The Business of Ski Resorts</a>. Retaining/recruiting seasonal talent, how climate change affects the business, and how resorts make money. Includes coverage about Whistler.
* WeChat opens "<a href="https://36kr.com/p/5292845">broadcast live sales</a>", a way for merchants to offer LIVE video shopping experiences. Launch your own home shopping network from your mobile phone.
* Seinfeld fan? The Seinfeld theme song <a href="https://twitter.com/jeremyburge/status/1229378614824701958?s=21">was recorded separately for each episode</a>.
* Another <a href="https://twitter.com/yashar/status/1229383312776388609?s=21">jetpack video</a>, look at the climb.. Bananas.
* Apparently Michael Jackson isn't dead he's a <a href="https://twitter.com/ogug8/status/1231098235911122946?s=21">wrestler in Brazil</a>
* Fortnite: the anti-weed, anti-sex, anti-TV, anti-alcohol, anti-smoking solution parents have been seeking for decades. <a href="https://mcusercontent.com/6b98493cc2e9ddaae1c981401/images/2977ba87-5dfe-4131-9d54-cbcca62db1fe.jpeg">Check this graph</a>
* Dating a new partner? <a href="https://twitter.com/joeymulinaro/status/1231656263647473665?s=21">Post meeting parents interview</a>


</div>
<hr>

<div class="container">


<h2>Personal</h2>


<strong>Books:</strong><br>
Trying to slow down, spend more time with each book, and re-read chapters

* Finished: <a href="https://amzn.to/3aWPnru">Mindset</a> for a second time
* Reading:  <a href="https://amzn.to/2uRO8t0">The Untethered Soul</a>, <a href="https://amzn.to/2vsMVsn">The Great CEO Within</a>, 



<strong>Film / TV:</strong><br>

* <a href="https://www.imdb.com/title/tt6751668/">Parasite</a> - loved this
* <a href="https://www.imdb.com/title/tt8946378/">Knives out</a> - fun, but 20 minutes too long
* <a href="https://www.imdb.com/title/tt0289043/">28 days later</a> - covid 
* <a href="https://www.imdb.com/title/tt0088763/">Back to the Future</a>
* <a href="https://www.imdb.com/title/tt7203552/">The Morning Show</a> - Jennifer is a smoke show


<strong>Music:</strong> <br>


* <a href="https://open.spotify.com/track/41L3O37CECZt3N7ziG2z7l?si=IxfVIr_ASxWpnazcg7ucmg">JB - Yummy</a>
* <a href="https://open.spotify.com/track/7jIujRjK5JKNrMCcAvYUTN?si=DzZchP_0RyaRCxYCSvywsg">Dr Dre - Keep Their Heads Ringin</a>
* <a href="https://open.spotify.com/track/2wZrjfH6TjAT8KNtArEaq0?si=ok-zhJnlTqm2qv3FfIoBQA">David Penn - The Power</a>
* <a href="https://open.spotify.com/track/1DWTFcfjHR8I98KSRk2ntW?si=9MTpE3hLQC2yS9YPs1LtJg">Aretha Franklin - Jump to It</a>


<p>That's all.. Let me know what you liked, want to see more or less of.
<br>
Kenny<br>
<a href="http://twitter.com/allbombs">twitter.com/allbombs</a> </p>
<br>



</div>
<hr>

<br>
</div>


<div class="container">

<h2>Photos from February</h2> 


<img alt="fam jam photo " src="/March-Newsletter-Issue-05/fam-jam-photo.jpg" class="full-width">
<img alt="sister baby" src="/March-Newsletter-Issue-05/sister-baby.jpg" class="full-width">
<img alt="nice boat yo " src="/March-Newsletter-Issue-05/nice-boat-yo.jpg" class="full-width">
<img alt="granville island bridge" src="/March-Newsletter-Issue-05/granville-island-bridge.jpg" class="full-width">




</div>
