---
title: if instagram stories launched in 2013 would it look like this
post_title: "If Instagram Stories launched in 2013, would it look like this?"
post_description: "In this medium post I want to share some thoughts on the challenges builders faced, and my own version of “Instagram Stories” from 2013. Long before Snapchat and IG launched what everyone uses today."
og_title: "If Instagram Stories launched in 2013, would it look like this?"
og_description: "In this medium post I want to share some thoughts on the challenges builders faced, and my own version of “Instagram Stories” from 2013. Long before Snapchat and IG launched what everyone uses today."
tw_title: "If Instagram Stories launched in 2013, would it look like this?"
tw_description: "In this medium post I want to share some thoughts on the challenges builders faced, and my own version of “Instagram Stories” from 2013. Long before Snapchat and IG launched what everyone uses today."
image: if-instagram-stories-launched-in-2013-would-it-look-like-this/letswatch.it-homepage-screenshot.png
date: 2018-02-26 08:16:32
---


<div class="container">
	<img alt="screenshot of airbnb annoucning stories" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/airbnb-announcing-stories-screenshot.png" class="full-width">
	<br>
<p>Earlier this morning <a href="https://twitter.com/ProductHunt/status/999865144078471168">Airbnb announced stories</a>. Youtube and a few others recently jumped on the story train too. It looks like “stories are the new news feed”? Videos show higher engagement and conversion rates. It’s the new norm. Everyone needs to record and consume video stories.</p>

<p><strong>Text updates are too basic, long live the future of video!!!</strong>

<p>Six years ago I spent significant time thinking about the future of video. </p>

<blockquote>How are consumers going to use and interact with personal videos? How do you capture emotion and share those moments with friends and family?</blockquote>

<p>In this medium post I want to share some thoughts on the challenges builders faced, and my own version of <strong>“Instagram Stories” from 2013</strong>. Long before Snapchat and IG launched what everyone uses today.</p>

</div>
<hr>
<br>
<div class="container">
<p>In 2013, photo UGC (User Generated Content) was just taking off. Most web based services compressed images to just 640x480 in size (low image quality). Phone’s had limited storage, poor camera resolution, and lacked high speed cellphone network coverage. For startups, bandwidth and storage was incredibly expensive. For small startups, it was capital intensive and just plain sucked.

<p><strong>The 2013 Landscape:</strong></p>
<p>Instagram was fairly new and exclusive to iPhone. You could post photos and select from a handful of filters. There was no video, direct messaging capabilities, explore, or ability to tag friends in photos. Android was unavailable and it was mostly early adopters. Just a handful of my personal friends had signed up. Early days.
Snapchat was focused around chat. Messages would disappear. There was no feed, videos, or group messages. It was used by teenagers with inexpensive phones.</p>

<p>Flickr was the preferred platform for sharing photographs. Youtube and Vimeo were the major video players.</p>

<p>Many startups were trying to figure out video broadcasting; creating a single video stream and broadcasting that stream across the globe. A few startups were aggregating videos from Youtube + Vimeo, the biggest one being chill.com.</p>

<img alt="screenshot of chill.com back in 2013" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/chill.com-screenshot.png" class="full-width">
<figcaption>Chill.com — aggregated content from vimeo and youtube.</figcaption>

</div>
<hr>

<div class="container">
<p>It was early days for video. I was excited to explore new ways people could interact and engage with personal videos and started “<a href="http://letswatch.it">Lets Watch It</a>.
</div>


<img alt="screenshot of letswatch.it" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/letswatch.it-homepage-screenshot.png" class="full-width">
<figcaption>The homepage for http://letswatch.it</figcaption>


<div class="container">
<h2>What was I building?</h2>
<p><strong>Discover and create interactive video threads with friends and family.</strong></p>

<p>In LWI, a collection of videos was a “video thread”. We wanted to replace text based facebook news feeds and VBB forums with a method for people to create, respond, and consume using just video. No more text based comments. <b>Everything would be video based</b>. It would surely be more engaging and interesting to share real videos with your face, emotions, and your surroundings?.</p>

<h2>An example camping video thread:</h2>
</div>

<img alt="camping trip story thread" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/video-thread-screenshot-desktop.png" class="full-width">
<figcaption>Pack your bags. We’re going camping with Boris, Master P, and yours truly!</figcaption>

<div class="container">
<p>At that time, I thought video threads  or video threading was pretty novel. Facebook lacked user permission, privacy, and multiple comment threads.</p>

<p>Posting photos and videos on the internet was an emerging trend. People were camera shy. I thought privacy was going to be the biggest factor for adoption.</p>
<div>


<img alt="lets watch it video clip settings" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/lets-watch-it-share-settings.png" class="full-width">
<img alt="lets watch it private video settings" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/private-video-settings.png" class="full-width">


<div class="container">
<p>Clearly, I was totally wrong. When it comes to video privacy, very few care. Everyone posts videos to IG, Snapchat, and Youtube with the end goal of reaching as many people as possible. Ease of use, mobile first, and distribution was the biggest factors for wide spread adoption.</p>
</div>
<hr>


<div class="container">
<h2>Video Threads<h2>

<p>Start your own video thread by recording a video, sharing another video link, or sharing a website. Include a quick introduction of what and why it’s being shared. Respond with your own video.</p>
</div>
<img alt="create new share video thread lets watch it" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/share-video-thread-lets-watch-it.png" class="full-width">
<figcaption>Starting a video thread in Lets Watch It</figcaption>

<hr>

<div class="container">
<h2>Technology — Desktop vs Mobile</h2>
<p>Why are all the screenshots desktop based? We figured desktop would be the best place to start. Android wasn’t an option because of inconsistent camera quality. iPhones were the mobile first platform of choice. They had superior cameras, native video compression built into the operating system, and distribution via the Apple App Store.

It came down to iOS vs Desktop? Regardless of platform, recording and streaming video content was hard. Why? Buffering. CDN’s weren’t popular, 3G cell phone coverage, upload speeds, server side compression, etc..
We weren’t aggregating video, but wanted to offer users the ability to quickly create videos. What platform would have the lowest barrier for video creation? Without video creation, the entire product was dead.
It would be too difficult (expensive) to build both a mobile and desktop application. We needed to pick one, and decided on desktop.
</p>

</div>

<br>
<img alt="record video with space bar" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/record-video-with-space-bar.png" class="full-width">

<figcaption>Recording video: hit spacebar to start and stop recording.</figcaption>

<hr>
<div class="container">
<h2>Smart TV Adoption</h2>

I experimented and even <a href="https://vimeo.com/41586183">built a few social tv applications</a>. Services available for any smart screen, or internet enabled television. Samsung was paving the way, but every manufacturer was building their own developer ecosystem of “Smart TV Apps”. The equivalent of the Apple App Store. Hardware was problematic.</p>


</div>
<hr>



<div class="container">
<h2>Video Threads To Replace Email and Text Messages?</h2>
<p>We thought _and still think_ Video Threads will replace text messages and email. Recording a video is faster and more personal. Especially if cameras are added to every part of your daily life — phones, computers, tvs, cars, bathrooms, kitchen, drones, etc..</p>
</div>

<img alt="video camping thread" src="/if-instagram-stories-launched-in-2013-would-it-look-like-this/camping-trip-thread.png" class="full-width">

<figcaption>Attaching my fav waterpark and potential campsite for group discussion</figcaption>

<div class="container">
<p>Another big miss was around the video creation process. Text overlays and filters are huge part of the storytelling process, and the magic of video based UGC. We definitely missed that.</p>
</div>
<hr>


<div class="container">
<h2>
Closing</h2>
<p>In the summer of 2013, I stopped working on the proejct. Why? Youtube.</p>
<p><strong>I didn’t see a way that we could compete against Youtube.</strong></p>
<p>Snapchat and IG weren’t on my radar. I thought IG would stay true to photograph enthusiasts. I thought Snapchat was a Whatsapp/Groupme competitor, a major thorn for telecommunication firms.</p>

<p>Youtube had the infrastructure, technical resources, userbase, and were starting to explore playlists and video replies.</p>


<strong><a href="/if-instagram-stories-launched-in-2013-would-it-look-like-this/Letswatch.it-wireframes-2013.pdf">The PDF Walk-through of Video Threads</a></strong>
</div>

<hr>

<div class="container">
<h2>Feedback</h2>
<p>What do you think? Were you working on a video based product? How would you launch stories in 2013? Would you do anything different than Snapchat and Instagram?<p>
</div>

<hr>

<br>


