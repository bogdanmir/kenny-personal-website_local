---
title: "List of Social Impact And Social Good Web3 Projects"
post_title: "List of Social Impact & Social Good Web3 Projects"
post_description: "Are you looking to get involved with a web3 NFPs (Not For Profit), Social Good, or Social Impact project? Use this blog post to find a suitable project that mateches your interest levels and start making an impact today"
og_title: "Autozen Automotive Marketplace Raises Round"
og_description: "Are you looking to get involved with a web3 NFPs (Not For Profit), Social Good, or Social Impact project? Use this blog post to find a suitable project and start making an impact today"
tw_title: "Autozen Automotive Marketplace Raises Round"
tw_description: "Are you looking to get involved with a web3 NFPs (Not For Profit), Social Good, or Social Impact project? Use this blog post to find a suitable project and start making an impact today"
image: List-of-Social-Impact-Social-Good-Web3-Projects/social-preview.png
date: 2022-03-15 12:17:02
---


<div class="container">



<p>Are you looking to get involved with a web3 NFP (Not For Profit), Social Good, or Social Impact project? Use this blog post to find a suitable project and start making an impact today!</p>


<p>
1. Intro<br>
2. What are DAO’s + Web3? Why should you care?<br>
3. Dao List - Airtable<br>
4. How to get involved + Ways to contribute<br>
5. Submit Your DAO Resource<br>
6. Closing<br>
</p>

</div>
<br>
<div class="container">
<h2>1. Intro</h2>


<p>Finding a worthwhile social impact group is difficult regardless of whether you are looking to learn, contribute 5-20 hours per week, gain fulltime employment, join a DAO, or make a donation. The term social impact or social good is vague; is there an area that lights you up the most? Between Google, Twitter threads, NFTs, medium, mirror, and discord, it's easy to get lost in the web3 sauce. Understanding what lights you up and how to cut through the noise is key. Joyce and I built out an Airtable list of social impact projects and are eager to showcase our findings! We hope this resource is educational and saves you time!</p>

</div>
<br>
<div class="container">
<h2>2. What are DAO’s + Web3? Why should you care?</h2>


<p>For those of you just beginning to step foot into the world of cryptos and web3, a DAO—-decentralized autonomous organization—is essentially a community that operates independent of a centralized authority (e.g. government). The technical definition of DAO might be difficult to grasp your head around, so let's think about DAOs in other ways. A lot of the time it is a Discord channel (group chat), but it is also so much more than that...It is a community and culture dedicated to a shared purpose. A network of people working as a collective.
</p>


<p>Here is what a couple of key leaders in the field have to say about DAOs:<br>

- <a href="https://twitter.com/Cooopahtroopa">Cooper</a> views DAOs as “the best way to learn about web3”. It’s a place where you will learn to buy tokens, use Discord, and vote, all while surrounded by a community of similar-minded peers eager to help. The best part is, you don't need any experience to start a DAO or be involved in one, there is something out there for everyone. You can read what Cooper has to say about DAOs <a href="https://coopahtroopa.mirror.xyz/_EDyn4cs9tDoOxNGZLfKL7JjLo5rGkkEfRa_a-6VEWw">here</a>.<br>

- If you prefer reading about DAOs, <a href="https://twitter.com/Cooopahtroopa">David</a> has a great book for you called Impact Networks, one of the best resources to learn about the fundamentals of DAOs.<br>

- <a href="https://twitter.com/mariogabriele">Mario</a> believes that DAOs are absorbing the internet. It is the home for the internet's most gifted, it controls billions in digital assets, it is where bright minds meet and collab, and it is defining cyberculture. To dive a little deeper, you can read what Mario wrote about DAOs <a href="https://www.readthegeneralist.com/briefing/dao">here</a>.<br>
</p>

<p>DAOs can then be categorized into different types, mainly technically-oriented or socially-oriented DAOs. Although a large proportion of DAOs are technical ones focused on building in the crypto space, there are various socially-oriented DAOs aimed at bringing people together. There has been an emerging trend of DAOs focusing on social impact, bridging the power of the internet with important social causes. One of the major ways DAOs are contributing to social impact is through fundraising and providing donations to charitable causes.</p>

<p><i>
One big part of social impact DAOs is being able to give to charities via crypto. Donating through Bitcoin, ETH, and other cryptocurrencies has become a popular option for many organizations. If you are interested in donating crypto to nonprofits, charities, or other mission-driven organizations, it may be worth looking into <a href="https://thegivingblock.com/donate/">https://thegivingblock.com/donate/</a>.</i>
</p>



<p>
Apart from donations, many DAOs work together to establish funding to support impact-driven projects or start-ups. DAOs also focus on posting content surrounding certain social justice issues, for example, climate change or education.</p>

<p>The list below is not extensive but it might be a good place to start if you would like to dig deeper.</p>

- **Gitcoin** — Gitcoin aims to build an internet that is open source, collaborative, and economically empowering by funding projects, building community, and making learning resources accessible. Want to get involved? You can start by joining their Discord or contributing to https://gov.gitcoin.co/.<br>

- **Klima DAO** — Klima DAO drives climate action via carbon-backed, algorithmic currency- the KLIMA token. Klima DAO is building an open-source, transparent community that will leverage the power of Web3 to deliver immediate and measurable climate-positive impact. The best way to participate in Klima DAO is through their <a href="https://discord.gg/klimadao">Discord</a>.<br>
<br>


</div>
<br>

<div class="container">
<h2>3. Dao List - Airtable</h2>


<p>Take a look at this list we've created of DAOs, creators, podcasts, etc. doing interesting things within the social impact crypto and web3 space. It's a good starting point for filtering by interest levels and navigating to a group that matches your jam. 
</p>


<br>
<iframe class="airtable-embed" src="https://airtable.com/embed/shrYla7y2pQ10AZny?backgroundColor=yellow&layout=card&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

<br>
<p>View in full screen --> <a href="https://airtable.com/shrYla7y2pQ10AZny">https://airtable.com/shrYla7y2pQ10AZny</a>.</p>
<br>



</div>
<br>
<div class="container">
<h2>4. How To Get Involved + Ways to Contribute</h2>

<p>I'm a huge fan of 'learn by doing'. Rolling up your sleeves and jumping in. What does that mean? Visiting the groups mission, reading the whitepaper, about us page, the founding team member's background, and joining discord to introduce yourself. Once inside, chat with other participants, hear how they contribute and don't be shy to ask roadmap questions. </p>

<p>I recommend starting slow and working backwards with the end goal in mind? Starting with your own lens, and then working through the group lens. What are your personal goals and how do those overlap with each group? Why is this cause important to you? Does this project have a long-term plan that makes sense, or are they trying to cash in on the web3 hype cycle? The crypto and web3 space is greasy and like any industry has bad actors. What are people in twitter saying about this group? Use your street smarts! Be careful connecting your wallet to any of these groups.</p>

<p>Other questions: <br>

- How much time and what resources do you have available (skills, time, network, money, memes, etc)? <br>

- Do you want to join a larger more established group, OR work within a smaller crew where you'll have more impact but potentially face more uncertainties about the longevity of this project succeeding. </p>

<p>Completing a small bounty or roadmap task is a great place to start. Common rewards include NFT's, tokens, coins, or other perks. After completing the first bounty, you might work on a second, or allocate more time and energy to the group. Alternatively, you might stop there and look elsewhere.</p>

<p>Sidebar - i'm looking for someone to help manage this Airtable. Is that you? Reach out.</p>

</div>
<br>
<div class="container">
<h2>5. Submit Your DAO Resource</h2>


<p>Are you the founder, or part of another social good group? We'd love to hear from you. Please submit your group or drop me a note via <a href="https://kenny.is">twitter dm</a>.<br>
<p>Submit your resource --> <a href="https://airtable.com/shrNuGc8YbDJGKaiY">https://airtable.com/shrNuGc8YbDJGKaiY</a></p>




</div>
<br>
<div class="container">
<h2>6. Closing</h2>


<p>We are just at the early stages of Web3 and DAOs, yet it has already made significant progress across our culture and society. With more emphasis on social good projects shortly, we can undoubtedly expect to see lots of potential. Are you still stuck, and need some guidance, drop a note in the comments! </p>

<p>If you enjoyed this post, or know someone that wants to get invovled in social impact projects, please share this post!</p>


</div>

<hr>

<div class="container">
<strong>About</strong><br>
Courtesy of Kenny Grant</a> via Viral Foundry</a>.<br>
<i><a href="https://viralfoundry.com">Viral Foundry</a> is a social impact foundry that's community-funded. If you enjoyed this content, please donate or purchase NFTs. We are looking for soulful product builders, designers, marketers, and good people. Send me a <a href="https://kenny.is">twitter dm</a> to join our discord or hear more details.</i></p>


</div>



