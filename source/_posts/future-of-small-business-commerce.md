---
title: future of small business commerce
post_title: "Future of Small Business Commerce"
post_description: "In 1994, Pizza Hut was the first website to offer online ordering of pizzas. They exclusively serviced Santa Cruz, California, but it marked the first time a website accepted online payments. E-commerce was born."
og_title: "Future of Small Business Commerce"
og_description: "In 1994, Pizza Hut was the first website to offer online ordering of pizzas. They exclusively serviced Santa Cruz, California, but it marked the first time a website accepted online payments. E-commerce was born."
tw_title: "Future of Small Business Commerce"
tw_description: "In 1994, Pizza Hut was the first website to offer online ordering of pizzas. They exclusively serviced Santa Cruz, California, but it marked the first time a website accepted online payments. E-commerce was born."
image: future-of-small-business-commerce/future-of-small-biz-commerce.jpg
date: 2014-04-29 03:03:02
---


<div class="container">
<p><i>(This blog post is an extended version of a contribution I made to Street Fight Mag. Read the original <a href="http://streetfightmag.com/2014/04/30/five-trends-that-will-shape-the-future-of-local-commerce/">here</a>.)</p></i>
</div>
<hr>

<div class="container">

<br><br>
<p>In 1994, Pizza Hut was the first website to offer online ordering of pizzas. They exclusively serviced Santa Cruz, California, but it marked the first time a website accepted online payments. E-commerce was born.</p>

<p>Fast forward 20 years and consumers no longer bat an eyelid placing their credit card details into a web browser to purchase products online. It's safe, secure, and so convenient. Year after year, we see hockey stick growth as consumers spend more money online than ever before.</p>


<img alt="screenshot of airbnb annoucning stories" src="/future-of-small-business-commerce/future-of-small-biz-commerce.jpg" class="full-width">

<p>It’s an obvious transition that’s turning the offline shopping world upside down. Experts have called this for years, but it’s finally become a reality, as both small business owners and big box outlets are forced to close doors because of showrooming and online competition.</p>

<blockquote>“There’s finally a common enemy. Someone that we didn’t take seriously. An evil force that must be dealt with” -- <u>Retail Store Manager, Vancouver, Canada</u></blockquote>

What we’re really talking about is the war against Amazon and the radical shift in consumer buying behaviour. Behind closed doors at every major big box outlet, senior executives are scrambling to build strategies to not only compete, but simply survive. Some call it a <a href="https://www.usatoday.com/story/money/business/2014/03/12/retailers-store-closings/6333865/">blood bath</a>, I would describe it as the next Klondike Gold Rush -- endless opportunities for those that quickly adapt and implement change.

Keep ahead of the pack by rethinking the shopping chain and looking for early signals as it relates to your business, big or small. Here are some predictions on where I see the future of commerce. Don’t get left behind, grab your shovel and get panning.
<br>
</div>
<br>
<hr>
<div class="container">
<br>


<p><strong>Marketplaces For X</strong><br>
Marketplaces are the new shopping mall. Why walk around for hours on end, when you can view millions of products from your mobile device, tablet, or desktop, anywhere in the world?</p>

<p>We’ll continue to see new kinds of marketplaces in new verticals with new experiences ( currently in marketplace 1.0 territory -- tons of innovation and exciting possibilities). VCs will continue to heavily invest in this area. Greylock Partners and several other VCs have created teams devoted to finding the next mobile mammoth marketplace; more craigslist-killers. New service based marketplaces -- plumbers, cleaners, carpenters, bartenders, event planners, travel agents, entertainers, etc.</p>


<p><strong>Tumbleweed Shopping Malls</strong><br>
Serious struggles ahead for shopping malls. Big box outlets will continue to close doors and focus efforts online. Those big stores drive foot traffic and support other chains and independent small businesses. High vacancy rates could drive malls out of business (ghost malls), or drive new experiences and opportunities. Eg. -- speciality malls, rezoning for office space (desirable and cheap with food/drink/ample parking), restaurant courts (think Vegas, replacing food courts with restaurant courts), Amazon/ecommerce pickup outlets, new types of shopping experiences (see virtual shops below). Malls located in heavily populated areas/major cities/attractions/travel hubs may not see this shift.</p>


<p><strong>Shopping Malls Servicing Stores</strong><br>
Online marketplaces offer tools and services for their retailers to sell more products online, shopping malls need to follow suit to compete. That could include marketing services, collaborated events or sales, and providing whatever services their store owners require (shipping fulfillment for all stores in the mall?)</p>


<p><strong>Food & Beauty Ripe for Ecommerce</strong><br>
Online grocery and beauty commerce are now feasible. All of your favorite big chains will offer online ordering and delivery. Monthly “box of x” subscription services will fade out, more focus on local + health/organic + environment. Expect to have local delivery men/women for all of the major grocery chains (bi-weekly deliveries by Walmart).</p>


<p><strong>Influx of Delivery Providers</strong><br>
New shipping providers in the delivery space. UPS, Fedex, DHL, etc., will be unable to handle the volume of same day delivery. We’ll see more options for local couriers/delivery companies and major retailers partnering with these providers to fulfill orders within hours of purchase. Companies can start to strategically place popular selling items throughout different areas of the city for quicker delivery times. This won’t work well for rural areas, but more applicable for major cities. Bikes + smart cars for the next 5 years. By 2025, we’ll see self driving delivery vehicles (drones + self driving cars).</p>


<p><strong>Local Retailers as a Supply Source</strong><br>
Instead of the factory/head office shipping ecommerce orders, they’ll reroute the order to the customers’ local town where a local business can ship the product. We’re still 5 years out, but think auto-dealers -- they can all see each other’s inventory, especially similar branded outlets (eg. Ford).</p>


<p><strong>Bufferbox for X</strong><br>
Bufferbox provides consumers the convenience of picking up their online purchases at kiosk locations, 24/7. We’ll start to see companies placing their lockerbox in strategic locations, and even within stores (Eg. -- Amazon lockerbox within Whole Foods).</p>


<p><strong>Local Distribution As A Service</strong><br>
Brands and small businesses will work with local ambassadors to provide marketing distribution on retainer/affiliate lead basis. Think product placements with celebs, but instead using 100s of influential locals to reach your target audience. Youtube = the biggest ecommerce marketing channel ever.</p>








<p><strong>User Generated Content = Gold</strong><br>
Continue to see more brands and businesses leveraging social media to advertise and showcase products (integrate consumer generated photos and videos into your ecommerce and review pages).</p>


<p><strong>Mobile Shopping Tools/Apps</strong><br>
Big chains will offer companion tools for on/offline shopping. This already happens today, but better tools for customers and business owners. Walk down the aisle with companion apps that can price check, read reviews, manage your shopping list and ask for help. Who’s going to make the best companion app where groups/family can collaborate on shopping lists?</p>


<p><strong>Exclusive Product Lines</strong><br>
Huge push for exclusive product lines. Big box outlets forced to spend big dollars experimenting and creating new product lines. New entrepreneurial hubs to incubate new product lines sponsored by big box chains. Great time for makers. Outlets are willing to experiment and take on more risk.</p>


<p><strong>Interactive Shops & Shopping Experiences</strong><br>
As shopping malls close, businesses work together to deliver new shopping experiences. In other words, new ways to attract customers and keep them in store/ on premise for as long as possible. Focus on building environments where people want to be seen and heard. That could be expensive decor (think Vegas casinos), live performers, cool hangout areas, live product keynotes for brands, and fashion shows.</p>

These experiences are extended to online interactive showrooms that customers access via desktops, tablets, mobile devices, or gaming consoles. Each platform offers new ways for consumers to discover and interact with products. This could be as simple as google hangouts with your favorite shop, or choose your own shopping adventure (stores offering different layouts, themes to shop from, or even virtual shops).</p>


<p><strong>Personalized Shopping Assistants</strong><br>
Big data with online shopping behaviour, in-store behaviour, and past purchase history leads to some interesting opportunities for marketers, both on and offline. This new marketing method should be used for world class customer support, unfortunately this super power will be abused; iBeacons will pave the way.</p>


<p><strong>Social Proof / Couponing</strong><br>
Expect to walk down the aisle, or browse products with local and relevant messaging as it relates to you, your network, and your community. More focus on social networks, reviews, referrals, community, and how people you know and trust make purchases. Coupons and unique offers sent strategically.</p>

<blockquote>“Get $5.00 rebate if you provide video review within 30 days of using product.”</blockquote>

<p>Traditional:<br>
Great products + service + customer support = loyal customers + long tail WOM marketing.</p>

<p>New Model:<br>
Great products + service + customer support + social proof = long term success.</p>

<br>
<br>


<p><strong>Packaging Innovation + Environmental Impact</strong><br>
More focus on the environment and responsible packaging of goods and services. Socially responsible companies are rewarded with long term loyal customers. Look at Wholefoods or Lululemon.</p>


<p><strong>Local Matters</strong><br>
City Halls compete to build entrepreneurial hubs (tax incentives, discounted rent, and support systems). Entrepreneurship is rewarded; locals want to support other locals. Food will pave the way (50 mile diet) and other verticals will follow. Businesses will collaborate to support each other with new types of marketing campaigns and local events.</p>


<p><strong>Video Commerce</strong><br>
Video is the most powerful medium in the world. End-to-end from advertisements, product demos, purchase authorization, delivery confirmation, and customer support.</p>


<p><strong>Live Video Shopping Channels</strong><br>
Brands offer live webinars where users can see new product lines and interact with makers. Think HSN (Home Shopping Network), but for big brands and marketplaces. Brands could run 24/7, or specific times during the day/week.</p>


<p><strong>Popup Stores</strong><br>
Online businesses and marketplaces start popup stores in strategic locations (mall, conferences, events, etc). Indochino is a great example -- they tour cities across North America to showcase their product line and allow customers to get a personal fitting. We could see kickstarter style popup stores where new or potential product lines are tested/sold in short term locations.</p>


<p><strong>Happy Customers = Everything</strong><br>
Despite the new wave of marketing tools and techniques, businesses need to get back to basics. Focus on customer service, up-selling, speedy customer support, and customer happiness scores. Differentiate yourself against Amazon and your competitors. Be unique, tell your story with your unique personality and brand, but always deliver happy customers.</p>
<br>
<br>

<p>Where do you see the future? Let me know in the comments below</p>


</div>

<hr>

<br>


