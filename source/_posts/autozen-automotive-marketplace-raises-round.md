---
title: "Autozen Automotive Marketplace Raises Round"
post_title: "Autozen Automotive Marketplace Raises Round"
post_description: "One of my clients is out of stealth and just announced a 4.2 million in seed financing. Autozen offers an easier, faster, and more transparent way for car owners to sell their vehicles"
og_title: "Autozen Automotive Marketplace Raises Round"
og_description: "One of my clients is out of stealth and just announced a 4.2 million in seed financing. Autozen offers an easier, faster, and more transparent way for car owners to sell their vehicles"
tw_title: "Autozen Automotive Marketplace Raises Round"
tw_description: "One of my clients is out of stealth and just announced a 4.2 million in seed financing. Autozen offers an easier, faster, and more transparent way for car owners to sell their vehicles"
image: autozen-automotive-marketplace-raises-round/auto-zen-screenshot-fb.png
date: 2021-02-27 18:17:02
---


<div class="container">

<img alt="Autozen marketplace logo" src="/autozen-automotive-marketplace-raises-round/autozen-logo.jpeg" class="full-width">

<p>One of my clients is out of stealth and just announced 4.2 million CAD in seed financing. Autozen offers an easier, faster, and more transparent way for car owners to sell their vehicles compared to the traditional car sale process. They aim to deliver a firm offer to sellers within 24 hours of listing.</p>

<p>As a venture builder, I helped name, design, vet providers, and scope product roadmap features for the online automotive marketplace.</p>


<p>Read the <a href="https://betakit.com/vancouvers-autozen-raises-4-2-million-cad-to-simplify-private-auto-sales/">press coverage</a> or visit <a href="https://autozen.com">autozen.com</a>. </p>

<p>Congratulations to Olivier Vincent and the rest of the team!</p>

</div>

<img alt="Autozen marketplace screenshots" src="/autozen-automotive-marketplace-raises-round/autozen-screenshots.jpeg" class="full-width">


<img alt="Autozen marketplace logo" src="/autozen-automotive-marketplace-raises-round/autozen-colours.jpeg" class="full-width">



