---
title: June Newsletter - Issue 08
post_title: "June Newsletter - Issue 8"
post_description: "Welcome to my personal newsletter. Welcome to Issue #08"
og_title: "June Newsletter - Issue 08"
og_description: "Welcome to my personal newsletter. Welcome to Issue #08"
tw_title: "June Newsletter - Issue 08"
tw_description: "Welcome to my personal newsletter. Welcome to Issue #08"
image: /June-Newsletter-Issue-08/kenny-and-boris-mann.jpg
date: 2020-05-30 01:49:04


---



<div class="container">
<p>I want to maintain my monthly newsletter updates, but it's hard to write anything when the world is so divided and 22 cities are rioting across America. How do we fix covid, healthcare for all, racism, earnings inequality, free speech, fact-checking, and Hong Kong?</p>

<p>Those seem far more important than my little newsletter updates. I have no solutions, but I suspect technology isn't playing the role it could.</p>

<p>I'm a bit bummed out about everything and will keep this newsletter short. I encourage you to chat with your partner, family, friends, and coworkers about everything that's going on. Some people refuse to discuss or get informed, but that in of itself is a privilege. </p>

</div>
<hr>

<br>
</div>

<div class="container">
<p><h2>Mobile Applications and Happiness</h2><br>

<p>The rankings below reflect data collected from a pool of 200,000 iPhone users. It appears clear to me, you want to build utilities that provide value without competing for time, your most precious resource.</p>
<img alt="list-of-most-happy-and-most-unhappy-apps.png" src="/June-Newsletter-Issue-08/list-of-most-happy-and-most-unhappy-apps.png" class="full-width">



<p>Imho, you want to build and support utility applications that solve real problems with paid subscriptions. Free apps fighting for your time are essentially smoking. Facebook are by far the worst offenders and should not be trusted. Or today as I type these words, I don't see a reason we should continue trusting them? Not until they start building consumer products responsibly without stealing happiness and time, our most precious possession.</p>
<p>TLDR - utility applications that consume 15 minutes or less of your daily screen time do not impact your happiness levels. Everything else might be detrimental to your personal temple. </p>
</div>
<hr>

<br>
</div>

<div class="container">
<p><h2>May Updates:</h2><br>



* Covid, healthcare for all, racism, earnings inequality, free speech, Mark Zucks, riots, etc
* Streaks: 135 days without coffee.
* On a 30-day x 60-minute meditation challenge with <a href="https://twitter.com/Nikita_Arora17">Nikita<a/>. We're just 3 days left in our challenge, anyone want to join me for June? 
* In your next life, you probably want to come back as a duck. <a href="https://cdn.substack.com/image/fetch/c_limit,f_auto,q_auto:good/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F6606f8a1-51a7-4247-acee-ede7c786893f_900x1068.jpeg">Here's why</a>.
* <a href="https://youtu.be/GoNN4wcBAGE">Breathtaking views</a> of western Scotland from the cockpit of a US Navy F/A-18E Super Hornet
* Unhealthy thinking patterns, a <a href="https://mcusercontent.com/6b98493cc2e9ddaae1c981401/images/9beb8088-96e5-49cd-98e1-dbe0166f1c0f.jpeg">visual cheat sheet</a>
* "<i><a href="https://mcusercontent.com/6b98493cc2e9ddaae1c981401/images/7101c906-3b46-46d5-bada-6b4c1bbcc8c0.jpeg">My neighbour is crowdsourcing their next house colour</i>" - awesome
* <a href="https://mcusercontent.com/6b98493cc2e9ddaae1c981401/images/599ae7ee-58e8-46b1-bdf1-3ac0bc0f186e.jpg">Kids are smart</a>. Today one of my 4th grade students renamed himself "reconecting ..." on our Zoom call and pretended that he was having internet issues to avoid participating in our lesson.
* A little dude <a href="https://twitter.com/rexchapman/status/1260243072908173313?s=21">making homemade quarantine pizza</a>...
* The <a href="https://twitter.com/brianroemmele/status/1258391772759076864?s=21">facial recognition trash bin</a>. China is so far ahead. 
<br>

<h2>Tech / Product stuff</h2> 

* Future of zoom calls and remote <a href="https://twitter.com/mynameisfraenki/status/1264735580627308544?s=21">meetings will prob look like this</a>. Sound effects, gifs, and video clips built into your video conference calls. Fast fwd to 30 seconds.
* <a href="https://onezero.medium.com/the-future-of-movie-theaters-might-look-a-lot-like-an-apple-store-185c941d02a5">The Future of Movie Theaters Might Look a Lot Like an Apple Store</a>
* Musical.ly's Alex Zhu on Igniting <a href="https://www.youtube.com/watch?v=wTyg2E44pBA">Viral Growth and Building a User Community</a> #Product
* Gustav Söderström, RnD Chief, gives a <a href="https://www.youtube.com/watch?v=jTM7ZCKEUGM&feature=youtu.be">brief history of Spotify</a> - youtube
* <a href="https://nav.al/build-a-team-that-ships">Build a team that ships</a>
* <a href="https://static1.squarespace.com/static/5ae0d0b48ab7227d232c2bea/t/5ba849e3c83025fa56814f45/1537755637453/BartRecSys.pdf">The Spotify recommendation algorithm paper</a>

<br>

</div>
<hr>

<br>
</div>

<div class="container">

<p>That's all for now. I will send a longer update in a few weeks. <br>Stay safe.</p>

<p>Kenny<br>
<a href="http://twitter.com/allbombs">twitter.com/allbombs</a><br></p>

</div>
<hr>

<br>
</div>

<div class="container">

<h2>May Photos</h2> 


<img alt="park drone wide " src="/June-Newsletter-Issue-08/park-drone-wide.jpg" class="full-width">
<img alt="park drone wide mothers day" src="/June-Newsletter-Issue-08/park-drone-wide-mothers-day.jpg
" class="full-width">
<img alt="park drone wide mothers day hollywood" src="/June-Newsletter-Issue-08/park-drone-wide-mothers-day-hollywood.jpg" class="full-width">
<img alt="kennyand boris mann" src="/June-Newsletter-Issue-08/kenny-and-boris-mann.jpg" class="full-width">


</div>























