---
title: second-screen-social-tv-concept-for-latin-billboard-awards
post_title: "Second Screen Social TV Concept for Latin Billboard Awards"
post_description: "Last year I was working on a Second Screen Platform to deliver synchronized content to smart phones, tablets, and laptops while watching television, movies, sports, or live events. I wanted to share some of those experiences with an example we built for The Latin Music Billboard Awards"
og_title: "Second Screen Social TV Concept for Latin Billboard Awards"
og_description: "Last year I was working on a Second Screen Platform to deliver synchronized content to smart phones, tablets, and laptops while watching television, movies, sports, or live events. I wanted to share some of those experiences with an example we built for The Latin Music Billboard Awards"
tw_title: "Second Screen Social TV Concept for Latin Billboard Awards"
tw_description: "Last year I was working on a Second Screen Platform to deliver synchronized content to smart phones, tablets, and laptops while watching television, movies, sports, or live events. I wanted to share some of those experiences with an example we built for The Latin Music Billboard Awards"
image: second-screen-social-tv-concept-for-latin-billboard-awards/join-the-live-show.png
date: 2013-07-03 19:16:00

---


<div class="container">

<blockquote>The app that never took the big screen</blockquote>


<img alt="join the live show" src="//second-screen-social-tv-concept-for-latin-billboard-awards/join-the-live-show.png" class="full-width">


<p>Last year I was working on a Second Screen Platform to deliver synchronized content to smart phones, tablets, and laptops while watching television, movies, sports, or live events. I wanted to share some of those experiences with an example we built for <strong>The Latin Music Billboard Awards</strong>. The application was never officially launched, but we felt the application and concept far surpassed other second screen competitors like <a href-"http://www.massrelevance.com/">Mass Relevance</a> and Twitter.</p>

<p><i>Note, the project and platform has been abandoned and all team members have since moved on. These concepts are from May 2012.</i></p>
<div>
<hr>
<br>


<div class="container">
<h2>The concept:</h2>
<p>We were building a companion iphone and android application to display secondary content for LIVE shows or prerecorded shows. The secondary content could be scheduled in advance, or populated on the fly. The platform offered tools for TV producers/hosts to white-label the application for their event, movie or show. In this case, users would download the Billboard Awards application to their smart phone.</p>

<p>All secondary content is syncrnoized and delivered to secondary devices (phone, tablet, laptop) as a secondary timeline to the main event. Think of a youtube player having two timelines. One for the video, and one for the secondary content on your phone while watching.</p>

<p>The goal is to obviously provide secondary content at the correct time, between 10 to 25 seconds after the live event. It’s imperative that nothing is sent too early or late, that could lead to some serious spoilers or confusion for viewers. We planned to syncronize the content by an audio trigger or time stamp. There’s pros and cons to both methods (local time zones, channel restrictions, live vs recorded, live vs dvd’s, pirated content, etc), but for the Latin Billboard example we planned to use universal time stamps (combo of server side and smart phones local timezone detection) to deliver content to secondary screens. We were using node.js to deliver the content.</p>

<br>
<h2>Here’s a quick rundown on the differences between syncronization methods:</h2>

<strong>Audio Sync</strong> - based on an audio water mark embeded to the video source or using an audio recognition provider (think shazam) that delivers show, episode, and show timestamp (not much in this space atm for exact time stamps). Content can be stored locally on phone, or downloaded from server during audio trigger and displayed within app.

<strong>Time Based</strong> - content is delivering based on users local time/timezone, or pushed to all devices globally. Examples of time based content:

* 6:09pm push content #03 - video of telly tubbies dancing
* 6:12pm push content #04 - ask the audience / poll
* 6:15pm push content #05 - display sponsored content during commercial

<p>The time based method works best as audio recognition has major flaws. We discovered some breakthrough methods to deliver content based on audio events, but I’ll save that for another rainy day… This demo is focused at LIVE real time pushes (everyone gets the same content regardless of location/timezone).
The content producer (person pushing content to secondary devices, usually the producer) can push out one of the following pieces of content:</p>

1. Text Update
1. Photograph
1. Video
1. Poll/Vote Now
1. Advertisement

<p>These updates appear on a timeline similiar to the twitter or facebook timeline, and support sharing to your favorite social networks.</p>

<p>Ok, lets’ get onto the demo. The Latin Billboard Awards uses the real time event theme (think sports, award show, election, etc). Here’s some wireframes and a walk through of the application from start to finsh:</p>
</div>

<img alt="second screen social tv homepage concept" src="//second-screen-social-tv-concept-for-latin-billboard-awards/second-screen-social-tv-homepage-concept.png" class="full-width">
<figcaption>Wireframe Walkthrough - Page 3</figcaption>
<div class="container">

<p><strong>Download Now - <a href="/second-screen-social-tv-concept-for-latin-billboard-awards/Latin_Music_Awards_-_Second_Screen_Concept.pdf">Full PDF Wireframe Walkthrough</a></strong></p>

<p>We also built a video example to display the full potential of the music awards. We took some highlites from the 2011 Billboard Music Awards and created a short video of some different events that fans and viewers would (hopefully) interact with. On the left hand side is the television/event, with the right hand side displaying the second screen (mobile device). During the actual event, we would recommend 30-60 seconds between each update. This video illustrates some potential secondary content during the actual event. The key to secondary content is the actual content... It has to be relevant and interesting. Apologies, I can’t find the HD version of the clip, but i’m sure you’ll get the jist with the video below. Also, this example uses mobile web app vs native app, but we could accomodate both as native app is hybrid app:</p>


<p><strong>Watch Video Demo now - <a href"http://player.vimeo.com/video/67352980">http://player.vimeo.com/video/67352980</a></strong></p>


<p>After joining the second screen experience, you can interact with friends and others interacting within the second screen experience. The secondary content is the baseline and the building blocks of second screen content.</p>

<p>Would love to hear what you think of our second screen experience. Will share the details of another second screen experience we produced for a television show in another post. Stay tuned..</p>

</div>
<hr>

<div class="container">
<p><i>Disclaimer - project and platform is now shelved. All video footage is copyright of Billboard. The intention of this post is to share some of our findings and visions for a true second screen experience and event. Talented team behind this? Max, Romain, Shih Oon. Follow them!</i></p>
</div>




















