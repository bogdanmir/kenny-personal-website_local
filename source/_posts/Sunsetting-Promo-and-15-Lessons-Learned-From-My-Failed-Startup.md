---
title: Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup
post_title: "Sunsetting Promo and 15 Lessons Learned From My Failed Startup"
post_description: "It’s officially over. I’m shutting down Promo and looking for a new home. I want to share details around what went wrong, and 15 lessons…"
og_title: "Sunsetting Promo and 15 Lessons Learned From My Failed Startup"
og_description: "It’s officially over. I’m shutting down Promo and looking for a new home. I want to share details around what went wrong, and 15 lessons…"
tw_title: "Sunsetting Promo and 15 Lessons Learned From My Failed Startup"
tw_description: "It’s officially over. I’m shutting down Promo and looking for a new home. I want to share details around what went wrong, and 15 lessons…"
image: Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/bali-will-smith-kanban-board.jpg
date: 2018-04-10 11:16:34

---


<div class="container">
<p>It’s officially over. I’m shutting down <a href="http://promo.co">Promo</a> and looking for a new home. I want to share details around what went wrong, and 15 lessons that you can apply to your early stage startup.</p>

<img alt="promo app for small business" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/Promo-smb-app.png" class="full-width">
<br>

<p>Before jumping in, I want to thank teammates, investors, family, friends, contractors, and everyone else for your support and feedback over the years. I take full ownership of the failed venture, and held on for as long as I could. I’m sorry for failing to produce a more fruitful outcome.</p>

<p><strong>What was the original vision for Promo?</p></strong>
<br>
<br>


<blockquote>“Launch a promotion across the internet in 90 seconds.” — <a href="https://drive.google.com/file/d/11yJtLmlFt-uHuN6Li1fW_tJB19CD8b7t/view?usp=sharing">pdf</a></blockquote>

<img alt="promo app flyer for small business owners" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/promo-main-copy.png" class="full-width">


<p>Promo is a website and promotion builder. We simplify the process for brick and mortar smb’s to successfully promote their business online. We provide a simple, fast, and effective way to promote their business online. Within just 90 seconds, they can post a promotion to their website, social media, and email marketing list. Manage sales and order fulfillment. All from a mobile phone, without the need of a desktop computer.</p>
</div>


<div class="container">
<p><strong>Who am I?</strong><br>
A solo founder from Vancouver, Canada with a background in product management, advertising, and marketing. Before working on Promo, I built digital products for 60k smbs and 800 sales reps at Ziplocal, an Internet Yellow Page company.</p>


<p><strong>Why are you shutting down Promo?</strong><br>
We’re out of runway. Promo doesn’t have traction, revenue, and were unable to find product market fit. I don’t see a path forward and trying to return capital to investors.</p>

<br>

</div>
<hr>



<br>
</div>

<div class="container">

<h2>15 Lessons From My Failed Startup:</h2>
<br>


<h2>1. Have Direct Contact With Customers. Only Use Resellers Once You Have Product Market Fit</h2>

<p>With a rough MVP, we signed a reseller partnership agreement with a Vancouver based IYP (Internet Yellow Page) company in a test pilot program. A 20 person sales team would resell Promo on a yearly subscription basis as a co-branded application.
We used the newly signed reseller agreement to raise an angel investment round to complete the build. Three months of runway to finish the build and onboard our first paying customers. We would raise a larger round following the test pilot program.</p>


<img alt="Pat throwing coin promo office resized" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/Pat-throwing-coin-promo-office-resized2.jpg" class="full-width no-margin">
<figcaption>Pat Dryburgh is a great designer and cool guy</figcaption>


<p>Six weeks into the build process, reseller requirements changed and our iOS app was behind schedule. The reseller wanted an Android application and a dashboard to manage account creation and customer support.</p>

<p>I scrambled to find money from family and friends, negotiated contractors to take half cash/half equity, and used freelancer.com to secure a senior Android developer. I allocated the remaining funds in escrow in a <i>“fixed bid project”</i>. You can guess what happened next. The Android build didn’t work properly, pushing our launch date months. During this time, the reseller requested a Wordpress Plugin. I was desperate to improve the product and produce a distribution channel for sales.</p>


<blockquote>Maybe we can compete against WooCommerce? Wordpress would be an acquisition target</blockquote>


<p>We started working on a WP plugin while Android was going to be ready <i>“any day now”</i>. Delays continued, we lost a few teammates, weeks turned into months, and our reseller lost all confidence in us.</p>

<p>We spent everything building out features for resellers, and very little on end customers, the ones that mattered most. To make things worse, customers couldn’t just signup on their own. The product was built for resellers without registration, signup flows, or on-boarding.</p>

<p>I paused working with our reseller, killed the Android version, and focused efforts validating Promo with iPhone. I would come back to android and our reseller once we found product market fit.</p>
<br>

<p><strong>Lesson</strong> — Don’t spend early stage money building features for resellers. Focus on your end customer, build a tight product that works, and only use resellers once you have product market fit, and ready for sales distribution.</p>
<br>
</div>
<hr>

<div class="container">

<h2>2. Have Enough Cash In the Bank, Duh!</h2>

<p>Ensure your runway supports the build and execution of the next milestone of your business. For years, I ran Promo with no more than 3 months of runway. From the start, we didn’t have enough cash to properly launch our reseller test pilot program and determine if Promo was a viable, or venture based business. Don’t leave the stable without enough food and water to reach your next destination.</p>
<hr>
</div>

<div class="container">
	
<h2>3. If you raise, Grow or Die. Don’t turn your venture based startup into a bootstrapped one, sitting in the middle of a desert.</h2>

<p>I put Promo in a dumb spot: raised 100k from angels, built an MVP product that was reseller focused, unstable, feature bloated, no direct customers, and with a limited runway. I didn’t want to raise additional money. Without traction or validation, what investor would touch Promo? How did I get here again? Where did all that money and time go, what did we prove? How can I get out of this mess?
Raising didn’t feel right. I wanted to validate the product, but I didn’t want to spend months fundraising. We desperately needed money to make payroll so I sourced a client project called Finstripe. We spent the next few months building a fintech MVP and replenished the company bank account extending our runway a few months.

<img alt="finstripe.com back end screenshot" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/finstripe.com-back-end-screenshot.jpeg" class="full-width no-margin">

<figcaption>Screenshot of the MVP we developed for Finstripe</figcaption>

Over the next 4 years, I ran a bootstrapped startup and product studio agency. Investors signed up for a venture based return. Unfairly, they received a flat bootstrapped startup in the middle of the desert, struggling to stay alive, without a path to mega growth. Don’t do that. If you raise venture, raise enough funds, keep raising and growing the business until you return 10–100x the initial investment.</p>

<p>Sidebar — Running or thinking about a bootstrapped startup? Justin Jackson writes regularly about <a href="https://justinjackson.ca/risks">common risks and challenges</a>.</p>
<br>
</div>
<hr>


<div class="container">
	
<h2>4. If You Must Client Service Work To Survive, Ensure Client Market Fit</h2>

<p>To keep the lights on, we developed random MVP’s for clients. These projects didn’t overlap with Promo or ecommerce. This caused a lot of headaches and made scheduling resources impossible. We would be in a 3 month Promo build phase, but our iOS developer was busy “keeping the lights on” with another client project. We wanted to work on Promo, but we needed the money. Building internal product was always a priority over client service work.
I started re-allocating bench time resources to new products: <a href="https://magic.promo.co/">Promo Magic</a>, <a href="https://affiliates.promo.co/">Promo Affiliates</a>, and <a href="http://unfurls.co/">other ideas</a> unrelated to Promo. Those products were easier to ship as they didn’t require mobile development resources. We could move faster, but this was a huge distraction for Promo and our team.</p>

<iframe width="800" height="350" src="https://www.youtube.com/embed/0FVtJZXA-oI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

<figcaption>Promo Magic — Pay For Performance Program for Makers</figcaption>

<p>My plan was to never run 3 businesses (agency + Promo + product studio), but after every 2–3 month client engagement, I was convinced this would be our last. We would move the product forward, start making revenue, and transition working full time on our internal products. We were product focused and would raise a large round to support Promo, become a mini Betaworks focused around ecommerce.</p>
<p>Dozens of people warned me about the agency + product studio trap, but I never listened. I bit off too much. You were right, I will eat humble pie.</p>
<p>Working at an early stage startup with constant uncertainty isn’t for everyone. Those that could handle it, struggled switching between a product mindset and client service mindset. Most importantly, we were unable to learn about our main target customer, sell through Promo, or any of our other products to that same customer base.</p>

<p><strong>Lesson</strong> — Avoid client service work while building product. But if you must, ensure it’s in the same vertical and customer base.</p>
<br>
</div>
<hr>



<div class="container">
	
<h2>5. Building Mobile Applications = Expensive</h2>

<p>Promo is big, not a simple little web application. Many moving parts and resources required to build and properly maintain. We required 5 developers — Backend, Frontend, iOS, Android, and Wordpress Dev. Avoid building as much as possible, leverage free tools, and if possible, build web applications instead of native mobile applications.</p>
<br>
</div>
<hr>



<div class="container">
	
<h2>6. If Early, Avoid Building Multiple Mobile Clients in Parallel (iOS + Android)</h2>

<p>Developing mobile applications is time consuming and expensive. We wasted a lot of money and time shipping the wrong features multiple times. For every new feature, we’d develop the new feature for 2 environments. IF you build the wrong thing, you’re now doing it twice, and bleeding additional money trying to maintain both platforms.
Launch new ideas and gather feedback in less time by focusing on just one mobile client. Ship your Android after your iOS app has product market fit and is 100% stable.</p>


<img alt="record promo ios screenshot" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/promo-record-promo-screenshot-ios.png" class="full-width">
<figcaption>screenshot of record screen process before evgeny starts developing<figcaption>

<br>
</div>
<hr>



<div class="container">
	
<h2>7. Competition is Necessity, and A Positive Driver</h2>

<p>Competing against Shopify, Wix, or dozens of heavily funded website builders was going to be tough. I distanced myself from our competitors. The closest thing to Promo was Gumroad by Sahil. A simple web based ecommerce enabler that raised 8.1m in venture. We were tackling smb’s from a different angle, but I felt like our competitors were too big to compete against. Not that I was scared of them, or didn’t think we could eventually take them out, but we weren’t directly competing with a specific feature, or dismantling a specific customer type that could be better served (like how airbnb dismantled the craigslist rental category).
We lacked a sense of competitive urgency that could further ignite myself, and other teammates on a day to day basis. I didn’t see a direct competitor. I failed to find, or create an artificial one for the team to rally against. I believe startups should do their own thing, but competition is healthy. It keeps everyone honest, and is a positive business driver.</p>
<br>
</div>
<hr>



<div class="container">
	
<h2>8. Hire The Best People At The Right Time</h2>

<p>Team is everything. Avoid hiring the wrong people, but it’s possible to hire the right people at the wrong stage. When you find the right people, don’t micromanage, empower them, let them build and improve on your vision.</p>
<br>
</div>
<hr>



<div class="container">
	
<h2>9. Obsess Over Sales & Distribution</h2>

<p>I spent too much time in the wrong trenches. I was absolutely obsessed with building product. I didn’t spend enough time working with customers, selling promo, or establishing what channels could deliver new leads (without resellers).</p>
<br>
</div>
<hr>




<div class="container">
	
<h2>10. Recognize Opportunity Cost & Check Your Ego</h2>

<p>I was hellbent on shipping the original vision of Promo. The market shifted and customer feedback wasn’t there, but I kept forging ahead. Sunk time, money, and effort clouded my judgement. My ego wanted to prove all the haters wrong. Check yourself, before you wreck yourself.</p>
<br>
</div>
<hr>


<div class="container">
	
<h2>11. Focus on Your Strengths</h2>

<p>Focus on what you’re good at. As a solo founder, I felt like I needed to know and understand everything. At times, I focused on “saving money” because we didn’t have it, completing tasks myself instead of outsourcing them, allowing me to focus on growing the business, or my areas of strength.</p>

<img alt="Focus on Your Strengths book" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/Focus-on-Your-Strengths.jpeg" class="full-width">



<br>
</div>
<hr>




<div class="container">
	
<h2>12. Find Your Visionary or Integrator</h2>

<p>Most successful companies have 2 key individuals: a visionary and integrator. Within your business, you’ll have more success if you clearly define, and source both roles. I recently read Rocket Fuel, and it shook me to my core (um, hello, where was this book 5 years ago?). I’m a visionary, and weak integrator. I’ve been playing both roles, ”spinning my tires”, driving myself and everyone on my team insane. Run this free quiz to determine if you’re an integrator or visionary, and then hire, promote, or join forces with your other half!</p>
<img alt="screenshot of kindle Find Your Visionary or Integrator" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/kindle-screenshot-kenny.jpeg" class="full-width no-margin">

<br>
</div>
<hr>

<div class="container">
	
<h2>13. Get Your Target Customer CLEARLY Defined. Get Purpose.</h2>

<p>I wanted Promo to appeal to a wide market of small business owners. The term SMB is far too vague and I wasn’t laser focused on a particular small business customer type. I wanted to build a big ass business that would change the world. Too much ambition, not enough purpose.</p>


<br>
</div>
<hr>


<div class="container">
	
<h2>14. Early Stage Remote First Startups Are Harder, Higher Risk, & Not For Everyone</h2>

<p>At the start, we had a small office, but eventually went fully remote. It’s hard to find teammates that are comfortable working at an early stage startup, juggling random client work projects in parallel, and working fully remote across multiple timezones. Teammates would go weeks or months working on different projects or clients in silo without working with other teammates. It was rare for the entire team to be working together on Promo, or a single project at once. They would feel bored, isolated, and disconnected from a team.</p>

<p>I tried to bring everyone together in a different location. We worked in bali for several months in a hacker villa, and rented a house in Jogja for 6 weeks. We were more productive under the same roof, laughed a lot, and felt more connected with everyone.
For truly early stage startups, especially those that are juggling client consulting work, and struggling to stay alive, I think you’ll have more success with a fixed local office, versus a remote one.</p>
<br>
</div>

<img alt="hacker house bali lets work on floor instead" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/bali-lets-work-on-floor-instead.jpg" class="full-width no-margin"><img alt="bali will smith kanban board" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/bali-will-smith-kanban-board-shrug.jpg" class="full-width no-margin">
<img alt="totally not staged startup team photo shoot ha" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/totally-not-staged-photo-shoot-ha.jpg" class="full-width no-margin">
<img alt="promo wireframe onboarding papers" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/promo-wireframes.jpg" class="full-width no-margin">
<img alt="remote team by pool" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/all-together.jpg" class="full-width no-margin">
<img alt="be a rockstar marketer at jogja pre flyer print" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/be-a-rockstar-marketer.jpg" class="full-width no-margin">



<hr>


<div class="container">
	
<h2>15. Work Smarter, Not Harder. It’s Okay to Have Fun</h2>

<p>As a remote first team, we worked from anywhere with internet. I would visit exotic locations, but end up spending the entire trip glued to my laptop. Monday to Friday was focused around keeping the business afloat, and weekends trying to move our products forward and planning the upcoming week. I was living the startup struggle porn life. I felt guilty and desperate to make things work at all costs. I refused to take weekends to recharge or have fun.</p>
<br>
</div>
<hr>

<img alt="Watercolour paint drawing by Rara and Dany bali hacker house" src="/Sunsetting-Promo-and-15-Lessons-Learned-From-My-Failed-Startup/bali-hacker-house-boom.JPG" class="full-width no-margin">
<br>

<figcaption>Watercolour paint drawing by Rara and Dany</figcaption>
</div>
<hr>

<div class="container">
<h2>Feedback</h2>
<p>Have you made the same mistakes? Do you have any feedback or additional thoughts on how I could improve my next venture? Would love to hear from you on twitter (<a href="http://twitter.com/@allbombs">@allbombs</a>) or in the comment section below.
What happens next?</p>

<p>I’m shutting down Promo and looking to return as much money as possible to investors. We didn’t find product market fit, but Promo (IP, the promo.co domain, or both) could be very valuable within another organization. I’m following this <a href="https://medium.com/@sampurtill/how-we-sold-our-startup-in-30-days-463b09efff68">30 day process</a> to sell Promo. Do you know someone that might be interested in Promo, or have some potential ideas? Please let me know <a href="mailto:kennywgrant@gmail.com">kennywgrant@gmail.com</a>.</p>

<p>Again, want to thank my teammates and support from investors, family, and friends.
Curious what’s next? Join my email newsletter.</p>
<hr>
</div>























