---
title: Shutting Down Unfurls
post_title: "Shutting Down Unfurls"
post_description: "Last weekend I refunded all active subscribers, logged into Heroku and deleted everything. It was bittersweet. "
og_title: "shutting down unfurls"
og_description: "Last weekend I refunded all active subscribers, logged into Heroku and deleted everything. It was bittersweet."
tw_title: "shutting down unfurls"
tw_description: "Last weekend I refunded all active subscribers, logged into Heroku and deleted everything. It was bittersweet. "
image: /shutting-down-unfurls/unfurls-website-previews.jpeg
date: 2020-04-25 11:16:34

---

<div class="container">
<img alt="unfurls website previews" src="/shutting-down-unfurls/unfurls-website-preview.png" class="full-width">
<br>

<p>Last weekend I refunded all active subscribers, logged into Heroku and <a href="https://youtu.be/XsCWG0VK6Xg">deleted everything</a>. It was bittersweet.</p>

<p>This product was particularly frustrating for a few reasons. I wrote a <a href="https://kenny.is/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/">lengthy blog post</a> on the problem space, had a successful product hunt launch (that killed our server on launch day), became featured on the Slack.com app directory, captured paying customers in the first week, but couldn't make this business work. I pivoted the product several times, but couldn't find a path to develop a profitable business. I take full responsibility for the failed venture. </p>

<p>I built this product to scratch my own itch.  And still, even after shuttering everything down, it feels like the problem still exists. No one has solved this.  </p>



<img alt="shakes iron fist gif" src="/shutting-down-unfurls/shakes-iron-fist.gif" class="full-width">
<br>

<p><strong>So why shut Unfurls down?</p></strong>

* Not profitable. I couldn't find enough paying customers.
* Server bills = expensive. Specifically, image generation.
* Service is dynamic, networks ever-changing, regular maintenance required to monitor and recreate design + development rules for 13 different networks. (example - If FB redesigns social news feed every week, we need to spend weeks recreating those changes).
* The product was starting to break.
* Unfurls is a distraction. I'm refocusing my efforts on products that closely align with human happiness and my life purpose.

<p><strong>Lessons</p></strong>

* Find paying customers quickly. I acquired paying customers within the first few weeks of launching unfurls.co and the unfurls Shopify application. 
* Just because you have paying customers doesn't mean you should keep going. I always felt the product solved a real problem, but I couldn't source a segment or repeatable distribution channel. Truth is, website previews are a pain in the ass, a real problem once you demonstrated the product and pain... 
But they just didn't care enough. Or more accurately, I failed to find a customer segment that cared enough. <a href="https://marker.medium.com/reflecting-on-my-failure-to-build-a-billion-dollar-company-b0c31d7db0e7">It doesn’t matter how amazing your product is, or how fast you ship features. The market you’re in will determine most of your growth</a>.
* Copycats. I'm competitive and believe whoever builds the best mousetrap should win. There were a few occasions that I felt a bit frustrated and heartbroken to see new signups turn into direct competitors. We launched, then noticed a few active users launched similar products a few months later. After reviewing our logs, we noticed some of those competitors had registered domains a few days after registering and using Unfurls. Perhaps a coincidence, but a good lesson. I reached out to a few of them to see if we could collaborate in some way. I was working on other products and more than anything just wanted to solve this problem of "standardizing website previews". I suggested an open-source approach, or other ways we could work together. Everyone has different motivations for building products... and that's okay. Competition is healthy. 
* Slack as a marketing and sales distribution channel. It totally bombed for us. 278 teams installed our app, but teams never really used it. I thought marketing based digital agencies would be our bread and butter. We <a href="https://www.youtube.com/watch?v=xRDAu26-6sw&feature=youtu.be">built a slack app</a> that I personally wanted to use with my marketing team. I was pumped. It was exactly what I wanted.It didn't come to fruition, marketing agencies on slack didn't share my enthusiasm or want to install the unfurls Slack app. I like Slack, but if I was doing it again, or advising others, I'd recommend using slack as a value add to your existing product, increase product user headcount, or way to improve retention / usability. The product should have some stickiness and regular use outside of the slack ecosystem. The app directory is not a good place to introduce your product or acquire new users. Example - Accountants that use XERO could save time using a slack integration. Best to acquire users online via marketing campaigns or xero.com, once sticky, improve the experience with slack integration that could drive additional seats or paid services. When was the last time you searched the slack app directory for a new app for your team? Exactly. 
* Tried to pivot unfurls to a <a href="https://www.youtube.com/watch?v=vSotlOyoqfY&feature=youtu.be">sticker generation tool</a> as the last hail mary.  We even pitched this through to Snap. It failed, but I'm glad we gave it a solid go. 

<hr>

<p>I'm still smiling and had lots of fun with this product. Unfurls is no longer a "hell yeah" project. I need to make space and reserve energy for HELL YEAH projects. And that's what i'm doing now.</p>

<p>Yikes. This has turned into a bit of a self-help therapy post 🏩. Thanks for listening and to all customers, teammates, or anyone that helped support unfurls, I appreciate it! </p>

<p>I suspect we might see Lala, Mato, and the rest of the crew in another incarnation...</p>

</div>



