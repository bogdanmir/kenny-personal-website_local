---
title: "New Product: Shopify Social Report"
post_title: "New Product: Shopify Social Report"
post_description: "Today we’re validating a new product called Shopify Social Report. Over 52% of Shopify stores have invalid social meta tags. Find out about our goals and how the new report works"
og_title: "New Product: Shopify Social Report"
og_description: "Today we’re validating a new product called Shopify Social Report. Over 52% of Shopify stores have invalid social meta tags. Find out about our goals and how the new report works"
tw_title: "New Product: Shopify Social Report"
tw_description: "Today we’re validating a new product called Shopify Social Report. Over 52% of Shopify stores have invalid social meta tags. Find out about our goals and how the new report works"
image: new-product-shopify-social-report/shopify-social-report-fb-preview.jpg
date: 2019-10-07 20:23:32
---


<div class="container">

<p>Today we’re validating a new product called <a href="https://unfurls.co/shopify-social-score">Shopify Social Report</a>. Over 52% of Shopify stores have invalid social meta tags. Many pre-built themes are completely missing these tags. Developers and store owners lack knowledge, and there’s no easy way to manage or correct them inside the Shopify dashboard. Especially for those running headless on Shopify Plus. If you sell hundreds or thousands of products, meta tag management is a pretty big pain in the butt.</p>

<p>Here are some imagery we used for our product hunt posting that shows more about the new product:</p>

<img alt="how social is your shopify store?" src="/new-product-shopify-social-report/SHOPIFY-SOCIAL-REPORT-PROUCT-HUNT-1.jpg" class="full-width">

<img alt="Find and correct problems. Drive more sales from social media" src="/new-product-shopify-social-report/SHOPIFY-SOCIAL-REPORT-PROUCT-HUNT-2.jpg" class="full-width">

<img alt="see how you compare against other shopify stores" src="/new-product-shopify-social-report/SHOPIFY-SOCIAL-REPORT-PROUCT-HUNT-3.jpg" class="full-width">

</div>
<hr>


<div class="container">
<p><strong>How did you get here? </strong></p>
	<p>We started working on a Shopify Metatags Application but quickly discovered store owners had no idea what social previews were, and how it impacts sales + conversion rates. We’re looking to educate before presenting a potential solution.</p>
<br>
<p><strong>How does the report work?</strong></p>
<p>It’s just like a website speed test, but for your social media tags. Within a minute, we’ll generate a report that audits your Shopify store. We crawl your homepage, product page, and one of your blog posts. We score each page, provide a live preview across 12 social networks, and provide best practice recommendations for each page type. The report is free to use and available today. </p>

<br>
<p><strong>Product Goals </strong></p>

<p>
What we’re trying to prove/disprove with this new tool:</p> <br>

* <strong>Education</strong>. Do they understand the problem? Can our v1 report educate them without any guidance or explanation?
* <strong>Demand</strong>. Do they want a solution for this problem? Are they willing to pay for us to manually fix this problem?

<p>
Talk to 50 Shopify store owners + 10 Shopify agencies. Ideally target stores that do heavy content marketing + care about branding. Avoid drop-shipping brands.</p>
<br>

<p><strong>Here’s an example screenshot of the report:</strong></p>
</div>

<img alt="shopify social report" src="/new-product-shopify-social-report/shopify-social-score-screenshot.png" class="full-width">
<hr>


<div class="container">
<h2>Call To Action</h2>
<p>Do you have a Shopify store? Run a free report <a href="https://unfurls.co/shopify-social-score/">here</a>.

<br>

Not on Shopify, but curious what a report looks like? View an <a href="https://unfurls.co/shopify-social-score/15704633359955/01-intro">example report</a>.</p>

<p>Let me know if you have any thoughts, ideas, or feedback.</p>



<hr>

<br>


