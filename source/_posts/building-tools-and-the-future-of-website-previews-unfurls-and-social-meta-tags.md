---
title: building tools and the future of website previews unfurls and social meta tags
post_title: "Building Tools and The Future of Website Previews, Unfurls, and Social Meta Tags"
post_description: "Let’s talk about website previews. The little previews you see when viewing or sharing links on social media networks, blogging platforms, and chat applications. They provide consumers valuable…"
og_title: "Building Tools and The Future of Website Previews, Unfurls, and Social Meta Tags"
og_description: "Let’s talk about website previews. The little previews you see when viewing or sharing links on social media networks, blogging platforms, and chat applications. They provide consumers valuable…"
tw_title: "Building Tools and The Future of Website Previews, Unfurls, and Social Meta Tags"
tw_description: "Let’s talk about website previews. The little previews you see when viewing or sharing links on social media networks, blogging platforms, and chat applications. They provide consumers valuable…"
image: building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-building-previews.png
date: 2018-02-14 11:16:34
---
<div class="container">
<p>Let’s talk about website previews. The little previews you see when viewing or sharing links on social media networks, blogging platforms, and chat applications. They provide consumers valuable information about the contents of a shared web-page, and help them decide if they should interact with them. Sadly, most website owners and marketing teams don’t realize website previews are customizable and carry huge impact on performance and driving traffic to your website.</p>
</div>

<img alt="good website preview and bad website preview comparation" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-1.png" class="full-width">

<figcaption>Sometimes called unfurls, oEmbeds, link previews, anchor previews, rich previews, open graph tags, preview cards, social cards, post previews, website tags, or social meta tags. There’s no standard naming convention, but we call them Website Previews.
</figcaption>

<div class="container">
<p>In this medium post, we’ll discuss the current landscape, problems, and opportunities with shared website links and doing business on social networks and messaging platforms.</p>
</div>

<hr>

<div class="container">
<p><strong>SPOILER ALERT</strong> — website meta tags are super hard, time consuming, and unsexy… But not anymore!! I’m announcing a new set of tools and looking for others to join me in the crusade to build the future of website previews across the internet.</p>
</div>

<div class="container">
<p><strong>In this mammoth blog post:</strong></p>

1. History of the web link. Why do links exist?
1. Bad Website Previews. Ouch.
1. Performance + Expected Results + Stats
1. Current Workflows, Problems, and Pitfalls
1. The 2018 Website Marketing Checklist
1. Existing Tools. Why + How I Got Here
1. New Product Announcement — Lookout by Unfurls.co
1. Our Mission & The future of Website Previews
1. Unanswered Questions
1. Call For Help

<hr>
<br>
<br>

<h2>1. History of web links and website previews.</h2>

What are web links? Why do they exist? They provide users an easy way to access another location, document, or file on the internet. Typically activated by clicking on a highlighted word, or image on a webpage. Here’s the evolution of the shared link on the world wide web:

* <strong>Static page, no web links, just raw text</strong>. Users navigate each file, folder, or server manually. Switching between pages or documents is time consuming, confusing, and difficult.

* <strong>Hypertext link.</strong> Linking pages or documents with hyperlinks. Users no longer need to manually navigate between pages, they can quickly navigate between pages by clicking the full page document hyperlink instead.

* <strong>Hypertext link with customizable anchor text.</strong> The ability for page owner, or poster to customize the text displayed for a hypertext link. Instead of using the full destination page of http://yahoo.com/news/usa/state20/document-tree/12323200238097232/views/12454177770.html, users can use a a short descriptive summary of the target location. It provides users additional context to the details of the destination document or webpage.

* <strong>Previews from search metadata.</strong> Some platforms abandon anchor text links, and develop a new website link experience leveraging search engine metadata. These previews are built using the SEO websites page title, page description, and page url.

* <strong>Controlled + customized website previews.</strong> Platforms create their own standard, and abandon search engine powered website previews. Search tags were problematic because page names and descriptions contain terms and phrases used to rank well within search engines (keyword stuffing), but appear unnatural and spammy in news feeds. Facebook develops a new website preview standard called Facebook Open Graph Tags, allowing website owners a method to specific an image, title, and description for each webpage they own and operate. Other platforms develop their own standard markup for website previews (Twitter Cards, oEmbed, etc).

</div>

<br>

<img alt="platform link evolution" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-2.png" class="full-width no-margin">
<figcaption></figcaption>

<br>

<div class="container">
<p><strong>Why do platforms care about website previews?</strong></p>

Publishers and platforms continually look to improve user experience, reduce spam/malware/fake news, and to encourage businesses to invest more time, resources, and advertising spends on their network.
<br>
<h2>2. Bad Website Previews. Ouch.. They hurt.</h2>

Every-time you share a website, platforms crawl the shared page looking for website preview tags. If no tags are found, platforms try to assemble previews using search engine tags, or by scraping images and text on the destination page. Sometimes it works, other times it’s disastrous. Here’s a few examples of bad website previews from the wild:
</div>
<br>

<img alt="website preview hall of shame" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-3.png" class="full-width">
<figcaption>All of these examples are easy to resolve. Want to add your preview to the hall of shame? Email us — <a href="mailto:sup@unfurls.co">sup@unfurls.co</a></figcaption>

<br>
<br>
<br>


<div class="container">
<h2>3. Performance + Expected Results + Stats</h2>

<h3>What sort of performance increase should I expect by installing website preview tags properly?</h3>

<p>Exact results vary based on vertical and nature of business. We recently encountered a content marketing campaign that improved clicks, shares, and conversions by 300% after implementing tags properly. This low hanging marketing fruit significantly improves traffic, clicks, shares, and the appearance of your brand. Everyone should take notice and implement.</p>

<h3>Stats. What percentage of websites use Website Preview Tags properly?</h3>

<p>Not many… Shocking stats incoming. According to a recent report by Simartech, most of the internet has failed to install and implement these tags correctly.</p>

* Over <strong>50% of the top 10k websites do not</strong> strong have FB Open Graph + Twitter Card website preview tags installed properly.

* Over <strong>57% of the top 100k websites do not</strong>  have FB Open Graph + Twitter Card website preview tags installed properly.

* Over <strong>63% of the top 1m websites do not</strong> have FB Open Graph + Twitter Card website preview tags installed properly

* Over <strong>78% of all active websites are missing these tags</strong>, or have them installed incorrectly.

<i>
	*Top websites defined not by sales, but by website traffic and visits.
	*1.33b websites actively registered at time of writing. 75% are inactive or parked domains
	*Sources <a href="https://www.similartech.com/categories/open-graph-tags">1</a>, <a href="https://www.internetworldstats.com/stats.htm">2</a>, <a href="http://www.internetlivestats.com/total-number-of-websites/">3</a>
</i>

<br>
<br>
<br>

<h2>4. Current workflows, Problems, and Pitfalls</h2>

<h3>Why aren’t website owners and marketing teams installing tags properly?</h3>

<br>

1. <strong>Education</strong> — no central hub to learn how to install tags, the individual best practices for each network, and the technical requirements for each of the ever-changing platforms. Teams are unaware of the technical possibilities and how previews impact their business.

2. <strong>Implementation</strong> — creating or updating tags requires collaboration from different departments and stakeholders. Implementation or edits require project managers to coordinate with design, marketing, development, and quality assurance. Too many people are required to make changes.

3. <strong>Tools</strong> — no workflow tools exist to manage the entire website preview tag process. Existing tools address specific parts of the process, simply don’t work, or are outdated. No central platform or education resource that ties everything together, and simplifies the entire process for teams.

4. <strong>Time</strong> — marketing teams are busy. Without education, implementation and workflow tools, the process is just too difficult, expensive, and time consuming.

<br>

<h3>Current process. A closer look at how teams struggle implementing Website Preview Tags</h3>

<i>(SKIP if you want to bypass to the tools section)</i>

<p>Over the last decade I’ve experienced the depths of website preview hell within several different organizations. Big or small, they face similar problems and bottlenecks. For today’s purpose, let’s walk through the process of shipping a new website project from start to finish. We’ll use a 25 person digital agency for our website journey.</p>

<p>Digital Agency starts working on new website project for Acme Company Inc.. With design and development complete, marketing steps up to implement website preview tags. Here’s the typical process in a marketing savvy organization from start to finish:</p>

<strong class="heading">1. Create Spreadsheet for Website.</strong>

<p>Marketing Manager creates excel spreadsheet. Lists out the details of new website. Page name, title, internal description, screenshots, link to brief/overview of each page.</p>

<strong class="heading">2. SEO Tag Creation.</strong>

<p>Marketing Manager sends spreadsheet to SEO (Search Engine Optimization) teammate. They review spreadsheet, conduct keyword research, and add search meta tag information (search engine title, description, updated url slug name, etc). With Search Engine Tags complete, they send back to the marketing manager for next steps.</p>

<strong class="heading">3. SEO Tag Review.</strong>

<p>Marketing Manager reviews newly created page name, title, description, new page slug, ensures it meets the goals of each page. If new page names or copy fail to meet page goals, they are bounced back to the SEO Manager for further revisions.</p>

<blockquote>60% of marketing teams do not continue to next step of creating website preview tags after reviewing SEO tags.</blockquote>

<p>Once Search tags are complete, most marketing managers pass the spreadsheet to the dev team for implementation and website launch.</p>

<p>Before passing to the development team, they should continue to the next step of creating the social and messaging platform tags.</p>

<strong class="heading">4. Website Preview Tag Creation.</strong>

<p>Marketing manager opens spreadsheet, creates new columns for previews (facebook, twitter, and oEmbed information). This process is very manual and time consuming. Most marketing managers are non technical. They might specify a title, description for each webpage, and for each tag (fb, twitter, oembed) and hope someone on the dev team can rewrite everything in proper html at a later stage in the process. Technical marketing managers write raw html within the spreadsheet.</p>
</div>

<img alt="promo sitemap 2017" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-4.png" class="full-width">
<figcaption>Sitemap that covers entire website, seo, and website preview details. Later passed to dev team for implementation.
</figcaption>
<br>

<div class="container">
<p>Building previews within excel is problematic. Marketing Managers write these tags without properly testing how they appear for each webpage and network. Spelling typos and invalid HTML are frequent, causing some webpages to crash or load improperly.</p>

<strong class="heading">5. Designer Create Website Preview Images For Each Page
</strong>

With website preview tags added to the excel spreadsheet, the marketing manager contacts design team to build preview images for each page. Each webpage* should have a unique website preview image that matches the page, page goal, page title, description, and follow the specs for each platform (Facebook Opengraph, Twitter Cards).

Designer reads creative brief, each page, title, and description for the website preview and creates unique images for each page. They upload to basecamp or save all images within a dropbox or google drive folder, and send back to the marketing manager for review.

<i>*Any page that consumers potentially share on social or messaging platforms.</i>


<strong class="heading">6. Image Review + Revisions</strong>

Marketing manager cross references excel with newly created images. Files are often labeled in confusing ways that are time consuming and problematic. Any images revision requests are sent back to designer.


<strong class="heading">7. Upload Image Assets</strong>

After image revisions, the Marketing Manager sends excel sheet and image folder to the development team for implementation. Dev team reviews the spreadsheet, and backend developer uploads all images to a public server.

<strong class="heading">8. Webpage Implementation
</strong>

Front End Developer opens each webpage in an HTML editor, cross references each webpage with excel spreadsheet, copies and pastes search engine tags and the website preview tags into header of each webpage

This step is the most critical step in the entire process, but extremely time consuming. Some pages are accidentally missed, tags are added to the wrong page, duplicated across the entire site, contain spelling typos, bad image url paths, or invalid HTML tags that in some cases actually crash the webpage, or entire site.

Once complete, the developer pushes to a QA staging area, or the website LIVE.

<blockquote>90% of marketing teams stop here, and fail to continue to the next step of testing and QA.</blockquote>

<strong class="heading">9. Quality Assurance + Revisions</strong>

Marketing Manager cross references excel spreadsheet while manually sharing each webpage on 10+ social networks and messaging platforms looking for errors. Each webpage takes 1–3 minutes to test, sometimes longer. For many, this is the first time the Marketing Manager has actually previewed the image, title, and description together in one view. Resulting in problems with either the image, page title length or description length. Any changes are sent back to the designer and developer for implementation.

<strong class="heading">10. Marketing Sign-off</strong>

After several revisions, Marketing Manager signs off on website previews and shares news with stakeholders and/or client.

<strong class="heading">11. Website Live. Stakeholders + Client Left Guessing</strong>

Stakeholders, clients, and teammates have no way to verify tags are implemented properly.

<strong class="heading">Additional thoughts:</strong>

* Small websites projects are more manageable, but still extremely time consuming, painful, and fail to use website preview tags best practices.

* Large websites with 10’s of thousands of pages do not create website previews manually. Many have no website previews whatsoever. The ones that do, use one generic template for their entire website. This is a rookie mistake. Companies spend millions of dollars on optimization, but fail to test, optimize, or monitor the performance of pages. Why? Lack of education, tools, and time.

<br>
<br>
<br>


<h2>5. The 2018 Marketing Website Checklist</h2>

The new updated checklist for Marketing Managers launching new websites, landing pages, and content marketing campaigns:

1. Marketing Brief
1. Copy
1. Design
1. SEO Tags
1. Search + Messaging Platform Preview Tags
1. Development
1. Cross browser testing
1. Cross device testing
1. Cross search, social, messaging testing
1. Website Preview Monitoring

<blockquote>Website Preview Builder, Testing, and Monitoring offered at unfurls.co</blockquote>
<hr>

<br>
<br>
<h2>6. Existing Tools + Why/How I Got Here?</h2>

<p>Before today, there wasn’t a dedicated resource for website previews. A few outdated blog posts and two specific platform debugging tools: <a href="https://developers.facebook.com/tools/debug/">Facebook Debugger</a> and <a href="https://cards-dev.twitter.com/validator">Twitter Validator</a>.</p>
    <div class="columns is-mobile no-multiline">
        <div class="column is-6 is-6-mobile">
            <img alt="facebook sharing debugger" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-5.png">
        </div>
        <div class="column is-6 is-6-mobile">
            <img alt="twitter card validator" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-6.png">
        </div>
    </div>
<figcaption>Facebook + Twitter Debugging Tools
</figcaption>

<p>Platforms build tools for their platform. They don’t build for the rest of the web. They focus on the end user and aren’t too concerned with how marketing teams launch and manage websites across the internet. I don’t blame them. How could they? It’s a conflict of interest and extends far past the platform they control.
</p>

<p>There needs to be a 3rd party resource that plays nice with website owners, website teams, all platforms, and most importantly the consumers each platform serves. We need workflow tools for websites of all shapes and sizes, with a central place for education and best practices.</p>

<p>I didn’t set sail 18 months ago on a mission to fix crummy website previews. I considered them an essential part of launching new marketing campaigns or owning a website, but I never imagined spending sooo much time and effort thinking about how people visit, share, and engages with websites across the internet. Here’s the story of how I got here, and details on what i’m building.</p>

<strong>June 2016 — Enough is Enough</strong>

<p>I was in the final stages of creating and testing website preview tags for a new website launch. In the struggle-club, switching back and forth between excel, sublime html editor, github desktop client, facebook debugger, twitter validator, and sharing links on my personal network. <i>“You’ve been hacked”</i> they said. My social network wasn’t too pleased with the 40+ new “website shares” within their feed. I palmed my left hand against my forehead and proclaimed, <i>“This is insanity! Why can’t I preview and test everything within one dashboard without spamming the world?”</i></p>

<p>And so it started. I sketched the first version. Marketers could preview a webpage across facebook, twitter, and a few other networks from one screen. Saving time and your news feed. A no brainer tool.</p>

<img alt="first wireframe of lookout" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-7.jpeg">
<figcaption>The first chicken scratch wireframe of Lookout
</figcaption>

<img alt="wireframe of lookout result page" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-8.png">
<figcaption>Lookout Result Page Version 0
</figcaption>

<strong>Messaging Platforms In Emerging Markets
</strong>

<p>Hailing from Vancouver, most of my friends and work peers use FB and Groupme. I wasn’t thinking too much about group messaging platforms or chat applications.</p>

<p>In August of 2015, I traveled to Europe and started spending significant time in SouthEast Asia. During that time, I discovered 2 new messaging platforms called Line and WeChat. Consumers were using these and other messaging platforms in new and exciting ways. Conducting real business. All within chat applications.</p>

<img alt="whatsapp chat" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-9.png">
<figcaption>Finding and negotiating accommodation in Indonesia. All from WhatsApp.
</figcaption>

<p>In 2011, I was a Product Owner developing new digital products for 60k small businesses across the United States. I researched, played, and launched new digital eCommerce solutions for SMB’s, but I was totally floored with what was going on in SE Asia. They had definitely leapfrogged North America in terms of chat messaging and small business commerce. Lots to learn, and chat applications were now on my radar and something I wanted to include in the tool.
</p>

<strong>Name and Brand?
</strong>

<p>My little tool had a name — Lookout. A simple way to preview your link across the internet and make changes from one browser tab. The preview tool grew from 3 networks to 5, then to 7 to include chat applications, and then to 12 different platforms. New applications that were important to businesses in other geography regions outside of North America.
</p>

<p>I thought Lookout was useful, but not something that could standalone. I had been traveling for over 2 years now. Visited over 30 countries, and living out of a suitcase. I was far removed from life in North America and meeting interesting people every week via tech startup conferences and local meetups. I was fascinated with locals. Watching how they use and leverage technology in a mobile only world.
</p>
<div class="gallery">
  <div class="columns is-mobile">
    <div class="column is-7 is-7-mobile">
      <div class="gallery-img-wrapper">
        <img alt="event in vietnam" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-10.jpeg">
      </div>
    </div>
    <div class="column is-5 is-5-mobile">
      <div class="gallery-img-wrapper">
        <img alt="restaurant in thailand" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-11.jpeg">
      </div>
    </div>
  </div>
  <div class="columns is-mobile">
    <div class="column is-5 is-5-mobile">
      <div class="gallery-img-wrapper">
        <img alt="working at cafe in indonesia" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-12.jpeg">
      </div>
    </div>
    <div class="column is-7 is-7-mobile">
      <div class="gallery-img-wrapper">
        <img alt="event in malaysia" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-13.jpeg">
      </div>
    </div>
  </div>
</div>
<figcaption>Photos from events in Vietnam, Thailand, Indonesia, Malaysia
</figcaption>

<p>I wanted to create a resource for technical marketing managers. People like myself. Covering news, interviews, and case studies across digital marketing. A resource to stay abreast of everything that’s happening locally, abroad, and within emerging markets. My existing marketing subscriptions provided an abundance of marketing bs and fluff. “If I create a monthly resource, will people actually read it?” Regardless, it would be an excuse to stay on top of digital marketing. I wanted to call the monthly marketing magazine Intel, short for Intelligence.
</p>

<p>I had an existing domain of MobileCavalry.com and decided to wrap Lookout and Intel under one brand. The vision; create small, but powerful digital tools for technical marketing teams on a subscription basis. Provide valuable, non bullshit marketing intelligence to teams struggling to stay on top of today, and fighting for tomorrow. Every month we’d deliver the latest digital marketing tips (with a global perspective), case studies, and at least one golden nugget of learning that you and your team could apply to your team and business.</p>

<p>We would develop small tools as we identified pain points, and offer them as part of the monthly subscription.</p>

<strong>Standing Out
</strong>

<p>Instead of stiff and traditional, I wanted the brand to stand out. Something completely different. “How can we build a community, connect with people, tell our story with global perspective, be totally different, and have a bit of fun?” We developed 5 characters. Everyone you would expect in a small digital team — a marketer, developer, designer, researcher, and leader. They were inspired by teammates, people I met on my travels, and some memorable times in Japan. “Could we create a small community and level up marketing teams? How can do this without distracting them?”
</p>

<img alt="the mobile cavalry team" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-14.png">
<figcaption>The Mobile Cavalry Team
</figcaption>

<strong>Feedback Round 1
</strong>

<p>With the first version of Lookout, Intel, and the Mobile Cavalry website complete, we setup 3 LIVE feedback sessions with marketing professionals in my existing network via Skype. Some feedback from those sessions:</p>

<blockquote>“For Lookout, why do I care? What makes a good website share, versus a bad website share? How can I make my website better?” — feedback 1
<br><br>
“It would be good to know if my link is good, or bad. How can I share this with my developer? How do we fix it?” — feedback 2
<br><br>
“I didn’t know these were adjustable” — feedback 3
</blockquote>

<p>From those sessions, we created a scoring system, shareable reports, and personalized tips for each Lookout result. We also created a website preview builder; a simple interface to build website previews for search engines, facebook, and twitter. Marketing Managers could now create perfect tags, and export the HTML in minutes.</p>

<strong>Feedback Round 2 — TIA Jakarta 2017</strong>

<p>In November of 2017, we brought Mobile Cavalry to Tech In Asia Jakarta. Visitors to our booth were confused with our offering, but after walking them through a demonstration, they quickly understood the value of Lookout. Many teams showcasing at the conference focused on eCommerce, delivery, and fulfillment. Problems that were solved 5–10 years ago in Europe and North America. This wasn’t the best audience for our product, but provided some new ideas around eCommerce within messaging platforms in SE Asia.</p>
</div>
<div class="container full-width-mobile">
  <div class="columns">
    <div class="column is-12">
      <div class="gallery-img-wrapper">
        <img alt="tech in asia jakarta" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-15.jpeg">
      </div>
    </div>
  </div>
  <div class="columns">
    <div class="column is-4 ">
      <div class="gallery-img-wrapper">
        <img alt="tech in asia card" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-16.jpeg">
      </div>
    </div>
    <div class="column is-4 ">
      <img alt="mobile cavalry booth at tech in asia" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-17.jpeg">
    </div>
    <div class="column is-4 ">
      <img alt="tech in asia conference room" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-18.jpeg">
    </div>
  </div>
  <div class="columns">
    <div class="column is-12">
      <img alt="tech in asia conference room" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-19.jpeg">
    </div>
  </div>
</div>

<div class="container">
<p>Mobile Cavalry & Lookout feedback from the conference:</p>
<blockquote>
“How do I create my first website preview? What are the best practices for my type of business” — Delivery based startup business owner
<br><br>
“Woah. I had no idea. My entire ecommerce website looked awful across messaging platforms. Every page needs help.” — ecommerce owner
<br><br>
“The characters are confusing. I missed the value of Lookout because of the characters” — investor
</blockquote>

<p>With feedback and new ideas, I returned to Vancouver and continued to build out some new features with the rest of my team. We set February as our official launch date.</p>

<hr>

<h2>7. New Product Announcement — Lookout by Mobile Cavalry</h2>

<p>On on February 14th , we pushed everything LIVE to Product Hunt and to our existing network of website owners and digital agencies.
</p>

<img alt="lookout page on product hunt" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-20.png">

<figcaption>Screenshot of our Valentine’s Day Product Hunt launch ❤</figcaption>

<p>Today, we’re attacking 3 areas teams face with website previews: education, workflow tools, and monitoring. These stem from problems we’ve personally faced as website owners, inside marketing teams, and as a platform.</p>

<p>Website Previews aren’t very sexy, but an important part of the internet. We hope some of these tools can help you:</p>

<h3>Lookout</h3>
<p>From 1 dashboard, test your webpage across 12 social and messaging platforms. Drive more traffic and conversions to your website, landing pages, and content marketing campaigns. Get report card and actionable tips to improve your website preview. <a href="https://mobilecavalry.com/lookout/">Run a free test on your website</a>, or view a sample report on http://ces.tech.</p>
<br>


<img alt="lookout result platform previews" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-21.gif">
<br>

<img alt="lookout result score" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-22.png">
<br>

<img alt="lookout result all networks score" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-23.png">

<br>

<figcaption>View this sample report — <a href="https://unfurls.co/lookout/results/a/1520578986">https://unfurls.co/lookout/results/a/1520578986</a></figcaption>

<br>

<h3>Website Preview Best Practices</h3>
<p>Become the master of Website Previews. Learn the most dos, and must nots. Our first educational resource that’s a must read for any website owner or website team. <a href="https://mobilecavalry.com/lookout/definitive-guide-to-website-previews-meta-tags">Read guide now</a>.</p>

<img alt="unfurls best practices" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-24.png">
<figcaption>Link — <a href="https://mobilecavalry.com/lookout/definitive-guide-to-website-previews-meta-tags">Website Unfurl Best Practices</a></figcaption>

<br>

<h3>Website Preview Builder</h3>
<p>A simple way to create website preview meta tags for Search, Social Networks, and Messaging Platforms. Get the perfect preview in less time and without any HTML errors. <a href="https://mobilecavalry.com/lookout/website-preview-builder-meta-tag-generator">Create preview now</a>.</p>

<img alt="unfurls website builder" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-25.png">
<figcaption>Link — <a href="https://mobilecavalry.com/lookout/definitive-guide-to-website-previews-meta-tags">Website Unfurl Best Practices</a></figcaption>

<br>

<h3>MC Slack Application</h3>
<p>Run Lookout requests with teammates. Get feedback, launch campaigns faster, and improve team workflow. <a href="https://mobilecavalry.com/slack-app">Add to slack now</a>.</p>
<br>
<iframe width="1032" height="580" src="https://www.youtube.com/embed/xRDAu26-6sw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>Link — <a href="https://mobilecavalry.com/slack-app">Add to slack now</a></figcaption>

<br>


<img alt="unfurls app on slack" src="/building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags/unfurls-26.png">
<figcaption>Link — <a href="https://mobilecavalry.com/slack-app">Add to slack now</a></figcaption>

<hr>

<p><i>We’re pausing our Intel magazine product to focus efforts on building tools for the future of website previews. This seems like a larger opportunity.</i></p>

<hr>

<br>
<br>

<h2>8. Our Mission & The Future of Website Previews</h2>

<br>

<h3>Our Mission</h3>
<p>Fix and build the future of interactive website previews. A third party that sits between website owners, website teams, publishers, platforms, and consumers. Be the catalyst that builds a new set of standards, tools, and best practices to better serve consumers.
</p>

<p>We’d love to develop open source components towards a new website preview standard. We need your help. Please see bottom of post.
</p>

<br>
<br>

<p><strong>What does “interactive website previews” actually mean?</strong><br>
Previews exist to help consumers. Messaging Platforms and Voice powered applications are on the rise. I believe these platforms will kickstart a revolution of website preview change.
</p>

<p><strong>Where and How to Build The Future?</strong><br>
Do you focus on the present, or just build for the future? In this case, we’re doing both, but starting with the present. Lookout helps website owners and website teams reach consumers on social and messaging platforms. We’re exploring SDK’s for publishers, and what comes next. The tools announced within this post are focused around education and workflow tools for website owners. Helping website owners drive the most possible traffic from social and messaging platforms.
</p>

<hr>
<br>
<br>


<h2>9. Unanswered Questions</h2>

<strong>Questions to unlock the future of website previews:</strong>

<ul>
  <li>How can website previews better serve consumers in these new environments? Why are existing platforms serving unsophisticated previews?</li>
  <li>How can website previews show more context to consumers? Fakenews is on the rise. How do we protect consumers?</li>
  <li>Why can’t we interact and complete transactions inside website previews? What platforms, interfaces, and payment methods will we use? Where do these transactions occur? Are websites necessary? Will social networks, messaging platforms, and voice platforms like Alexa and Siri replace eCommerce websites on the internet?</li>
  <li>What’s the best way for consumers to gather and share content? How should platforms improve this experience? How can website owners deliver information and better serve customers on these platforms?</li>
  <li>Can publishers serve consumers and monetize from a new website preview standard? How do business owners (the ones selling products online) and platforms work together to service their customers, the consumer?</li>

</ul>

<p>We’ve been thinking deeply about these questions. Feedback + open discussion welcome.</p>

<hr>

<br>

<h2>10. Call for help</h2>

<p>We need your help. Do you have direct feedback, ideas, or know someone that’s working on this problem? Have you “been there, and done that”? Are you personally interested in this problem? Please get in touch. We’re super keen to chat, and open to collaborate. My email address is <a href="mailto:kennywgrant@gmail.com">kennywgrant@gmail.com</a>, or I can be reached via <a href="http://twitter.com/allbombs">@allbombs</a> on the twitter.</p>

<p>That’s all for now. Visit <a href="http://unfurls.co/">unfurls.co</a> or join our <a href="#">newly created slack group</a> to give feedback, or say hello. Thanks for reading, bye for now!</p>
<br>

</div>

<br>


