---
title: April Newsletter - Issue 6
post_title: "April Newsletter - Issue 6"
post_description: "Welcome to my personal newsletter. Welcome to Issue #06"
og_title: "April Newsletter - Issue 06"
og_description: "Welcome to my personal newsletter. Welcome to Issue #06"
tw_title: "April Newsletter - Issue 06"
tw_description: "Welcome to my personal newsletter. Welcome to Issue #06"
image: /April-Newsletter-Issue-06/english-bay-sunset.jpg
date: 2020-04-01 07:00:09


---



<div class="container">
<img alt="covid wall mural art vancouver" src="/April-Newsletter-Issue-06/english-bay-sunset.jpg" class="full-width">


<p>I hope everyone's keeping safe. Vancouver is in good shape versus many parts of the world. How are you holding up? I'm rollerblading the seawall most days and just plugging away with work. I will spare you a with shorter monthly update. Kicked off 2 design projects last week.</p>

<p>Stay safe</p>


</div>


<hr>

<div class="container">


<h2>March Updates:</h2>

* Covid-19.
* Streaks: 76 days without coffee, lost my gym streak of 146 consecutive weeks 😭
"Less - Alcohol Tracker" mobile app from Kevin Rose - <a href="https://apps.apple.com/us/app/less-alcohol-tracker/id1484828892">https://apps.apple.com/us/app/less-alcohol-tracker/id1484828892</a>
* Ever see a <a href="https://mcusercontent.com/6b98493cc2e9ddaae1c981401/images/b149bf4b-52a9-479a-8367-87869c09fd40.jpeg">Winnebago Heli-Home</a> in the early 70s? Asking price was a cool $ 300,000. Only 8 were sold.
* Bookstore that sells one book. Morioka Shoten is a tiny bookstore of “<a href="http://www.takram.com/projects/a-single-room-with-a-single-book-morioka-shoten/">a Single Room with a Single Book</a>” in Tokyo
* <a href="http://analytics.podtrac.com/blog/2020/3/24/new-weekly-podcast-data-amid-the-covid-19-crisis">Audio Podcasts are down</a>, mobile app usage and video is way up
* Simulating the lost games as hyper-realistic audio broadcasts - <a href="http://nba.fm/">NBA.FM</a>
* Amazon has secretly been working on a <a href="https://www.businessinsider.com/amazon-secretly-working-on-cold-cure-2020-3?IR=T&utm_medium=email&utm_term=BII_Daily&utm_source=Triggermail&utm_campaign=BI%20Intelligence%20Daily%202020.3.10">cure for the common cold</a>
* <a href="https://www.bloomberg.com/news/articles/2020-03-11/augmented-reality-startup-magic-leap-is-said-to-explore-a-sale">Augmented-Reality Startup Magic Leap to Explore a Sale</a>
* A modern day <a href="https://twitter.com/jerm_cohen/status/1244364848231854080">quarantine love story</a>
* Enjoyed this <a href="https://pod.link/1434060078">podcast with former CPO Brian Norgardformer</a>, discussing the future of Augmented Reality and AirPods. 
* "<a href="https://www.i2cat.net/presentations-space-intersects-internet-workshop/">Space Intersects Internet</a>" Workshop (LEO + 5G/IoT constellations) LEO's (Low Orbit Satellites). Want to build the AWS of satellites? Early and exciting..


</div>
<hr>

<div class="container">


<h2>Design:</h2>

<p>Observation: Design of web and mobile applications is phenomenal across the board. Large companies that were once slow, are now fast.. and beautiful.</p>

<p>Banks, SaaS, government micro-sites, everyone and everything digital looks stunning. This is fantastic news for consumers. Challenging for digital entrepreneurs that leverage design as a major competitive advantage.</p>

<p>Digital Product Cycles - NA and Europe facing:</p> <br>

* Web 1.0 - any web service that's usable, or with a sales team
* Web 2.0 - social social social + social graph + social growth (design doesn't matter)
* Mobile 1.0 - anything usable, can look like whatever
* Web 3.0 - ux + design matters (startup beachhead)
* Mobile 2.0 - ux + design matters (startup beachhead)
* Mobile 3.0 - beautiful, perfect, fast ,secure, and without a glitch.. or I'll delete you
* Web 4.0 - mobile was our priority, but now let's clean up our website + desktop experience. Beautiful, perfect, fast, secure, and without a glitch.


<p>Over the last 6 to 12 months we've seen a boom of designer focused tools like Sketch, Figma, and Zeplin. These tools standardize and speed up the process of developing digital products. Meaning, all low hanging fruit related to mobile is officially done. Legacy unsexy businesses are entering, or finishing the Mobile 3.0 cycle. The next wave of competitive design, especially for early-stage startups:  3D, Audio, AR, VR, Space, Household, Food, and Health.</p>


<h2>Product:</h2>

<p>I'm working with a Vancouver based marketplace and postponed my travel plans to Asia and Europe. Under NDA, can't share much. Looking for a second consulting gig and wire-framing / sound-framing an audio based application.</p>

</div>
<hr>

<div class="container">


<h2>Personal:</h2>


<strong>Books:</strong><br>

* Finished: <a href="https://amzn.to/39zyDEN">Art Of War</a>, <a href="https://amzn.to/3aWPnru">Mindset</a> 
* Reading:  <a href="https://amzn.to/2yoJFQp">Principles<a/>, <a href="https://amzn.to/2vsMVsn">The Great CEO Within</a>,


<strong>Film / TV:</strong><br>

* <a href="https://www.imdb.com/title/tt1598778/">Contagion</a>
* <a href="https://www.imdb.com/title/tt0289043">28 days later</a>
* <a href="https://www.imdb.com/title/tt0463854">28 weeks later</a>
* <a href="https://www.imdb.com/title/tt0480249">I am Legend (alternative ending)</a>
* <a href="https://www.imdb.com/title/tt1798709">Her</a> - worth revisiting


<strong>Music:</strong> <br>

* <a href="https://whohears.com/">Crate diggers club</a> - a community of digital crate diggers, who share unfamiliar gems.
* Been djing some weekend on twitch! 


<p>That's all.. Let me know what you liked, want to see more or less of.
<br>
Kenny<br>
<a href="http://twitter.com/allbombs">twitter.com/allbombs</a> </p>
<br>

</div>
<hr>

<br>
</div>


<div class="container">

<h2>A few photos from March</h2> 



<img alt="iga empty shelves " src="/April-Newsletter-Issue-06/iga-empty-shelves.jpg" class="full-width">
<img alt="save on foodsbig lineup" src="/April-Newsletter-Issue-06/save-on-foods-big-lineup.jpg" class="full-width">
<img alt="iga plexi glass screen cashier" src="/April-Newsletter-Issue-06/iga-plexi-glass-screen-cashier.jpg" class="full-width">
<img alt="weed store still serving" src="/April-Newsletter-Issue-06/weed-store-still-serving.jpg" class="full-width">
<img alt="facetime hangouts" src="/April-Newsletter-Issue-06/facetime-hangouts.png" class="full-width">
<img alt="yo dj" src="/April-Newsletter-Issue-06/yo-dj-.jpg" class="full-width">
<img alt="stanley park sunrise bridge" src="/April-Newsletter-Issue-06/stanley-park-sunrise-bridge.jpg" class="full-width">
<img alt="lions gate bridge wow" src="/April-Newsletter-Issue-06/lions-gate-bridge-wow.jpg" class="full-width">
<img alt="english bay olympic statue sunset" src="/April-Newsletter-Issue-06/english-bay-olympic-statue-sunset.jpg" class="full-width">


</div>
