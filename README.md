## Installation
kenny small test

### Install Hexo
before installing Hexo, you do need to have a couple of other things installed first:
- Node js http://nodejs.org/
- Git

If your computer already has these. You can skip to the Hexo installation step.

Once all the requirements are installed, you can install Hexo with npm:

`$ npm install -g hexo-cli`

### Clone this repo to your local machine
you can clone this repo by running:
`$ git clone https://gitlab.com/shipmates/kenny-personal-website.git`

`$ npm install hexo --save`

### Starts a local server
run this:
`$ hexo s`

By default you can access the website at `http://localhost:4000/`

### Create new post
you can create new post by run hexo command or manually create new file on `/source/_posts`

to create new post with hexo command
`$ hexo new [post-name]` 

example:
`$ hexo new building-tools-and-the-future-of-website-previews-unfurls-and-social-meta-tags`

it will created .md file in `/source/_posts` and a folder with the same name where you can put asset files

to create post manually
create .md file and a folder with the same name in `/source/_posts`

### Media

Avoid uploading files directly to the `/public` folder. Files in that folder are generated automatically from `/source`. For post specific image/video, upload to a post specific folder for example: `/source/_posts/my-post-name/img1.jpg`.

### Deploy
you can just push to master branch to deploy the website, it will be automaticly deployed to netlify. before deploy you have to generate the website. run this following commands:

`$ hexo g`

then push to master branch

`$ git push origin master`

### Create redirect rules
To configure redirects, you can write redirect rule on `public/_redirects`

Each redirect rule must be listed on a separate line, with the original path followed by the new path or URL.
```
/home              /
/journal           /blog
/google            https://www.google.com
```
